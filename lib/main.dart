import 'package:flutter/material.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/sqlite_database.dart';
import 'package:onfly_teste/app/wrapper.dart';

import 'app/app_dependency_injector.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SqliteDatabase.instance?.init();

  final AppDependencyInjector appDependencyInjector = AppDependencyInjector();
  appDependencyInjector.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'OnFly - Test',
      theme: ThemeData(
        useMaterial3: true,
      ),
      home: SafeArea(child: Wrapper()),
    );
  }
}
