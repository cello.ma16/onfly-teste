import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/modules/authentication/presentation/pages/login_page.dart';
import 'package:onfly_teste/app/modules/home/presentation/pages/home/home_page.dart';
import 'core/services/auth_provider/auth_provider.dart';

class Wrapper extends StatelessWidget {
  final authProvider = GetIt.I.get<IAuthProvider>();

  Wrapper({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: authProvider.user,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.active) {
            if (snapshot.data == null) {
              return const LoginPage();
            } else {
              return const HomePage();
            }
          } else {
            return const LoginPage();
          }
        });
  }
}
