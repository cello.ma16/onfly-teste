import 'package:onfly_teste/app/core/dependencies/i_dependency_injector.dart';
import 'package:onfly_teste/app/modules/travels/travels_dependency_injector.dart';

import 'core/core_dependency_injector.dart';
import 'modules/authentication/authentication_dependency_injector.dart';
import 'modules/dashboard/dashboard_dependency_injector.dart';
import 'modules/expenses/expense_dependency_injector.dart';
import 'modules/home/home_dependency_injector.dart';

class AppDependencyInjector {
  final IDependencyInjector _coreDependencyInjector = CoreDependencyInjector();
  final IDependencyInjector _authenticationDependencyInjector =
      AuthenticationDependencyInjector();
  final IDependencyInjector _travelsDependencyInjector =
      TravelsDependencyInjector();
  final IDependencyInjector _expensesDependencyInjector =
      ExpenseDependencyInjector();
  final IDependencyInjector _dashboardDependencyInjector =
      DashboardDependencyInjector();
  final IDependencyInjector _homeDependencyInjector = HomeDependencyInjector();

  Future<void> init() async {
    _coreDependencyInjector.init();
    _authenticationDependencyInjector.init();
    _travelsDependencyInjector.init();
    _expensesDependencyInjector.init();
    _dashboardDependencyInjector.init();
    _homeDependencyInjector.init();
  }
}
