import 'package:onfly_teste/app/modules/authentication/infra/datasource/i_authentication_datasource.dart';

import '../../../../core/domain/entities/user.dart';
import '../../../../core/services/database/sqlite/daos/authentication/i_authentication_dao.dart';
import '../../exceptions/domain_exceptions.dart';
import '../../params/signin_params.dart';

class AuthenticationDatasource implements IAuthenticationDatasource{

  final IAuthenticationDao authenticationDao;

  AuthenticationDatasource({required this.authenticationDao});

  @override
  Future<User> signin(ISigninParams params) async {
    final resultMap = await authenticationDao.signin(params);


    if(resultMap == null) throw InvalidCredentialsException();

    return User.fromMap(resultMap);
  }
}