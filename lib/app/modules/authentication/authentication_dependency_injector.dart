import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/modules/authentication/domain/usecases/signin_usecase.dart';
import 'package:onfly_teste/app/modules/authentication/external/datasources/authentication_datasource.dart';
import 'package:onfly_teste/app/modules/authentication/infra/datasource/i_authentication_datasource.dart';
import 'package:onfly_teste/app/modules/authentication/infra/repositories/authentication_repository.dart';
import 'package:onfly_teste/app/modules/authentication/presentation/blocs/authentication/login_bloc.dart';

import '../../core/dependencies/i_dependency_injector.dart';
import '../../core/services/database/sqlite/daos/authentication/i_authentication_dao.dart';
import 'domain/repositories/i_authentication_repository.dart';

class AuthenticationDependencyInjector implements IDependencyInjector {
  final _getIt = GetIt.instance;

  @override
  void init() {
    //DATASOURCE
    _getIt.registerLazySingleton<IAuthenticationDatasource>(() =>
        AuthenticationDatasource(
            authenticationDao: _getIt.get<IAuthenticationDao>()));

    //REPOSITORIES
    _getIt.registerLazySingleton<IAuthenticationRepository>(() =>
        AuthenticationRepository(
            authenticationDatasource: _getIt.get<IAuthenticationDatasource>()));

    //USECASES
    _getIt.registerLazySingleton<ISigninUsecase>(() => SigninUsecase(
        authenticationRepository: _getIt.get<IAuthenticationRepository>()));

    //BLOC
    _getIt.registerFactory<LoginBloc>(() => LoginBloc(
        signinUsecase: _getIt.get<ISigninUsecase>(), authProvider: _getIt.get()));

  }
}
