import 'package:onfly_teste/app/modules/authentication/types/authentication_types.dart';

import '../../params/signin_params.dart';

abstract class IAuthenticationRepository {
  Future<SigninType> signin(ISigninParams params);
}