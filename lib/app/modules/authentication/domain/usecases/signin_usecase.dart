import 'package:onfly_teste/app/modules/authentication/domain/repositories/i_authentication_repository.dart';
import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';
import 'package:onfly_teste/app/modules/authentication/types/authentication_types.dart';

abstract class ISigninUsecase {
  Future<SigninType> call(ISigninParams params);
}

class SigninUsecase implements ISigninUsecase{
  
  final IAuthenticationRepository authenticationRepository;
  
  const SigninUsecase({required this.authenticationRepository});
  
  @override
  Future<SigninType> call(ISigninParams params) async {
    return await authenticationRepository.signin(params);
  }
}