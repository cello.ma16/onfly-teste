abstract class DomainException implements Exception {
  final String message;

  DomainException(this.message);
}

// class WrongMailFormatException extends DomainException {
//   WrongMailFormatException() : super("E-mail inválido");
// }

class InvalidCredentialsException extends DomainException {
  InvalidCredentialsException() : super("E-mail ou senha inválidos");
}

class UnexpectedException extends DomainException {
  UnexpectedException() : super("Um erro inesperado ocorreu, tente novamente mais tarde");
}