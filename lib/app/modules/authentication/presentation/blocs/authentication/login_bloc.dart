import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';

import '../../../../../core/services/auth_provider/auth_provider.dart';
import '../../../domain/usecases/signin_usecase.dart';
import 'login_events.dart';
import 'login_states.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final ISigninUsecase signinUsecase;
  final IAuthProvider authProvider;

  LoginBloc({required this.signinUsecase, required this.authProvider}) : super(LoginInitial()){
    on<LoginAuthenticateEvent>(_onAuthenticate);
    on<LoginToggleObscureTextPasswordEvent>(_onToggleObscureTextPassword);
  }

  Future<void> _onAuthenticate(LoginAuthenticateEvent event, Emitter<LoginState> emitter) async {
    final state = this.state as LoginInitial;

    emitter(state.copyWith(loadingButtonLogin: true));

    await Future.delayed(const Duration(seconds: 2));

    final params = SigninParams(email: event.email, password: event.password);
    final result = await signinUsecase(params);

    result.fold(
      (failure) {
        emitter(state.copyWith(errorMessage: failure.message, user: null, hasError: true));
      },
      (user) {
        authProvider.addUser(user);
        emitter(state.copyWith(errorMessage: null, user: user, hasError: false));
      },
    );
  }

  void _onToggleObscureTextPassword(LoginToggleObscureTextPasswordEvent event, Emitter<LoginState> emitter) {
    final state = this.state as LoginInitial;
    emitter(state.copyWith(obscureTextPassword: !state.obscureTextPassword));
  }
}