import 'package:equatable/equatable.dart';

import '../../../../../core/domain/entities/user.dart';

abstract class LoginState extends Equatable {}

class LoginInitial extends LoginState {
  final bool hasError;
  final String? errorMessage;
  final User? user;

  final bool obscureTextPassword;
  final bool loadingButtonLogin;

  LoginInitial(
      {this.hasError = false,
      this.errorMessage,
      this.user,
      this.obscureTextPassword = true,
      this.loadingButtonLogin = false});

  LoginInitial copyWith(
      {bool? hasError,
      String? errorMessage,
      User? user,
      bool? obscureTextPassword,
      bool? loadingButtonLogin}) {
    return LoginInitial(
        hasError: hasError ?? false,
        errorMessage: errorMessage,
        user: user ?? this.user,
        obscureTextPassword: obscureTextPassword ?? this.obscureTextPassword,
        loadingButtonLogin: loadingButtonLogin ?? false);
  }

  @override
  List<Object?> get props => [
        hasError,
        errorMessage,
        user,
        obscureTextPassword,
        loadingButtonLogin,
        obscureTextPassword,
        loadingButtonLogin
      ];
}
