abstract class LoginEvent {}

class LoginAuthenticateEvent extends LoginEvent {
  final String email;
  final String password;

  LoginAuthenticateEvent({required this.email, required this.password});
}

class LoginToggleObscureTextPasswordEvent extends LoginEvent {}
