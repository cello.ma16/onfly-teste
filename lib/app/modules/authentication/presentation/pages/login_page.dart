import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/utils/color_utils.dart';
import 'package:onfly_teste/app/modules/authentication/presentation/blocs/authentication/login_bloc.dart';
import 'package:onfly_teste/app/modules/authentication/presentation/blocs/authentication/login_events.dart';

import '../../../../core/utils/image_utils.dart';
import '../blocs/authentication/login_states.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final loginBloc = GetIt.I.get<LoginBloc>();
  bool loadingButton = false;
  bool obscurePassword = true;

  final TextEditingController _emailTextEditingController =
      TextEditingController();
  final TextEditingController _passwordTextEditingController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocConsumer(
        bloc: loginBloc,
        listener: (context, state) {
          if (state is LoginInitial) {
            if (state.errorMessage != null) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.errorMessage!),
                  backgroundColor: ColorUtils.errorColor,
                ),
              );
            }
          }
        },
        builder: (context, state) {
          if(state is LoginInitial){
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(ImageUtils.logoColored),
                    const SizedBox(height: 50),
                    TextFormField(
                      controller: _emailTextEditingController,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: ColorUtils.borderColor),
                          borderRadius:
                          const BorderRadius.all(Radius.circular(30)),
                        ),
                        hintText: 'E-mail',
                        hintStyle: TextStyle(color: ColorUtils.borderColor),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor, informe um e-mail';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _passwordTextEditingController,
                      obscureText: state.obscureTextPassword,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: ColorUtils.borderColor),
                          borderRadius:
                          const BorderRadius.all(Radius.circular(30)),
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            loginBloc.add(LoginToggleObscureTextPasswordEvent());
                          },
                          child: state.obscureTextPassword
                              ? const Icon(Icons.visibility_outlined)
                              : const Icon(Icons.visibility_off_outlined),
                        ),
                        hintText: 'Senha',
                        hintStyle: TextStyle(color: ColorUtils.borderColor),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor, informe uma senha';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 50),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: ColorUtils.primaryColor,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      onPressed: state.loadingButtonLogin ? null : () async {
                        if (_formKey.currentState!.validate()) {
                          loginBloc.add(LoginAuthenticateEvent(
                              email: _emailTextEditingController.text,
                              password: _passwordTextEditingController.text));
                        }
                      },
                      child: state.loadingButtonLogin ? const Center(child: CircularProgressIndicator()) : Text(
                        'Entrar',
                        style: TextStyle(
                            fontSize: 16,
                            color: ColorUtils.whiteColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }else{
            return const Center(
              child: Text('Erro ao carregar a página'),
            );
          }
        },
      ),
    );
  }
}
