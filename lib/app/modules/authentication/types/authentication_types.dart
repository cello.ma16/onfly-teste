import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/core/domain/entities/user.dart';
import 'package:onfly_teste/app/modules/authentication/exceptions/domain_exceptions.dart';

typedef SigninType = Either<DomainException, User>;