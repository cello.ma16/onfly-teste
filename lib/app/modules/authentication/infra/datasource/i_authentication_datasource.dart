import 'package:onfly_teste/app/core/domain/entities/user.dart';
import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';

abstract class IAuthenticationDatasource{
  Future<User> signin(ISigninParams params);
}
