import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/authentication/domain/repositories/i_authentication_repository.dart';
import 'package:onfly_teste/app/modules/authentication/infra/datasource/i_authentication_datasource.dart';
import 'package:onfly_teste/app/modules/authentication/types/authentication_types.dart';

import '../../exceptions/domain_exceptions.dart';
import '../../params/signin_params.dart';

class AuthenticationRepository implements IAuthenticationRepository {
  final IAuthenticationDatasource authenticationDatasource;

  AuthenticationRepository({required this.authenticationDatasource});

  @override
  Future<SigninType> signin(ISigninParams params) async {
    try {
      final user = await authenticationDatasource.signin(params);
      return Right(user);
    } catch (e) {
      return Left(e is DomainException ? e : UnexpectedException());
    }
  }
}
