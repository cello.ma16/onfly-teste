abstract class ISigninParams {
  String get email;
  String get password;
}

class SigninParams implements ISigninParams {
  @override
  final String email;
  @override
  final String password;

  SigninParams({
    required this.email,
    required this.password,
  });
}