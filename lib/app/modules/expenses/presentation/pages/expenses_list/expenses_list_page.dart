import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/presentation/widgets/expense_card.dart';

import '../../../../../core/presentation/widgets/base_page_body.dart';
import 'bloc/expenses_bloc.dart';
import 'bloc/expenses_event.dart';
import 'bloc/expenses_state.dart';

class ExpensesListPage extends StatefulWidget {
  const ExpensesListPage({super.key});

  @override
  State<ExpensesListPage> createState() => _ExpensesListPageState();
}

class _ExpensesListPageState extends State<ExpensesListPage> {
  final _bloc = GetIt.instance.get<ExpensesBloc>();


  @override
  void initState() {
    super.initState();
    _bloc.add(FetchExpensesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BasePageBody(
        title: "Despesas",
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 20),
              const Text('Suas últimas despesas',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              const SizedBox(height: 20),
              Expanded(
                child: RefreshIndicator(
                  onRefresh: () async {
                    _bloc.add(FetchExpensesEvent());
                  },
                  child: BlocBuilder(
                    bloc: _bloc,
                    builder: (context, state) {
                      if (state is ExpensesLoading) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (state is ExpensesLoaded) {
                        if (state.expenses.isEmpty) {
                          return const Center(
                              child: Text('Nenhuma despesa encontrada'));
                        } else {
                          return ListView.builder(
                            padding: const EdgeInsets.all(0),
                            itemCount: state.expenses.length,
                            itemBuilder: (context, index) {
                              return ExpenseCard(
                                expense: state.expenses[index],
                              );
                            },
                          );
                        }
                      } else {
                        return const SizedBox();
                      }
                    },
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
