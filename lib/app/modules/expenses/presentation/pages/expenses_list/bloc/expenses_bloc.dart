import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/get_expenses_usecase.dart';

import '../../../../../../core/domain/entities/expense.dart';
import 'expenses_event.dart';
import 'expenses_state.dart';

class ExpensesBloc extends Bloc<ExpensesEvent, ExpensesState> {
  final IGetExpensesUsecase getExpensesUsecase;

  ExpensesBloc({required this.getExpensesUsecase}) : super(ExpensesInitial()) {
    on<FetchExpensesEvent>(_onFetchExpenses);
  }

  Future<void> _onFetchExpenses(
      FetchExpensesEvent event, Emitter<ExpensesState> emit) async {
    emit(ExpensesLoading());
    final result = await getExpensesUsecase();
    result.fold(
      (failure) {
        emit(ExpensesLoaded(expenses: const <Expense>[]));
      },
      (expenses) {
        emit(ExpensesLoaded(expenses: expenses));
      },
    );
  }
}
