import 'package:equatable/equatable.dart';

import '../../../../../../core/domain/entities/expense.dart';

abstract class ExpensesState extends Equatable {}

class ExpensesInitial extends ExpensesState {
  @override
  List<Object?> get props => [];
}

class ExpensesLoading extends ExpensesState {
  @override
  List<Object?> get props => [];
}

class ExpensesLoaded extends ExpensesState {
  final List<Expense> expenses;

  ExpensesLoaded({required this.expenses});

  @override
  List<Object?> get props => [expenses];
}