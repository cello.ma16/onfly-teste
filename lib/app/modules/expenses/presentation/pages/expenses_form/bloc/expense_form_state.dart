import 'package:equatable/equatable.dart';

abstract class ExpenseFormState extends Equatable{}

class ExpenseFormInitial extends ExpenseFormState {

  final bool hasError;
  final String? errorMessage;
  final bool isLoadingButton;
  final bool success;

  ExpenseFormInitial({this.hasError = false, this.errorMessage, this.isLoadingButton = false, this.success = false});

  ExpenseFormInitial copyWith({bool? hasError, String? errorMessage, bool? isLoadingButton, bool? success}) {
    return ExpenseFormInitial(
      hasError: hasError ?? false,
      errorMessage: errorMessage,
      isLoadingButton: isLoadingButton ?? false,
      success: success ?? false
    );
  }

  @override
  List<Object?> get props => [
    hasError,
    errorMessage,
    isLoadingButton,
    success
  ];
}