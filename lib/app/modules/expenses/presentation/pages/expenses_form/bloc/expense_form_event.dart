import 'package:onfly_teste/app/core/domain/entities/expense.dart';

abstract class ExpenseFormEvent {}

class SaveExpenseFormEvent extends ExpenseFormEvent {
  final ExpenseCategoryEnum category;
  final double value;
  final String description;
  final DateTime date;

  SaveExpenseFormEvent({required this.category, required this.value, required this.description, required this.date});
}

class UpdateExpenseFormEvent extends ExpenseFormEvent {
  final Expense newExpense;
  final Expense oldExpense;

  UpdateExpenseFormEvent({required this.newExpense, required this.oldExpense});
}