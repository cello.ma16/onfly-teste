import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/update_expense_usecase.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';

import '../../../../domain/usecases/save_expense_usecase.dart';
import 'expense_form_event.dart';
import 'expense_form_state.dart';

class ExpenseFormBloc extends Bloc<ExpenseFormEvent, ExpenseFormState> {
  final IUpdateExpenseUsecase updateExpenseUsecase;
  final ISaveExpenseUsecase saveExpenseUsecase;

  ExpenseFormBloc(
      {required this.updateExpenseUsecase, required this.saveExpenseUsecase})
      : super(ExpenseFormInitial()) {
    on<SaveExpenseFormEvent>(_onSaveExpenseForm);
    on<UpdateExpenseFormEvent>(_onUpdateExpenseForm);
  }

  Future<void> _onSaveExpenseForm(
      SaveExpenseFormEvent event, Emitter<ExpenseFormState> emit) async {
    final state = this.state as ExpenseFormInitial;
    emit(state.copyWith(isLoadingButton: true));

    final params = SaveExpenseParams(
        description: event.description,
        value: event.value,
        category: event.category,
        date: event.date);
    final result = await saveExpenseUsecase(params: params);

    result.fold(
      (failure) {
        emit(state.copyWith(hasError: true, errorMessage: failure.message));
      },
      (expense) {
        emit(state.copyWith(success: true));
      },
    );
  }

  Future<void> _onUpdateExpenseForm(
      UpdateExpenseFormEvent event, Emitter<ExpenseFormState> emit) async {
    final state = this.state as ExpenseFormInitial;
    emit(state.copyWith(isLoadingButton: true));

    final params = UpdateExpenseParams(
        newExpense: event.newExpense, oldExpense: event.oldExpense);
    final result = await updateExpenseUsecase(params: params);

    result.fold(
      (failure) {
        emit(state.copyWith(hasError: true, errorMessage: failure.message));
      },
      (expense) {
        emit(state.copyWith(success: true));
      },
    );
  }
}
