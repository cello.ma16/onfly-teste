import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:onfly_teste/app/core/presentation/widgets/base_page_body.dart';
import 'package:onfly_teste/app/core/utils/color_utils.dart';
import 'package:onfly_teste/app/core/utils/formatter_utils.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_form/bloc/expense_form_bloc.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_form/bloc/expense_form_state.dart';

import '../../../../../core/domain/entities/expense.dart';
import 'bloc/expense_form_event.dart';

class ExpenseForm extends StatefulWidget {
  final Expense? expense;

  const ExpenseForm({super.key, this.expense});

  @override
  State<ExpenseForm> createState() => _ExpenseFormState();
}

class _ExpenseFormState extends State<ExpenseForm> {
  final _bloc = GetIt.instance.get<ExpenseFormBloc>();

  late DateTime selectedDate;

  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _valueController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  ExpenseCategoryEnum selectedCategory = ExpenseCategoryEnum.FOOD;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _setupData() {
    selectedDate = widget.expense?.date ?? DateTime.now();
    _dateController.text = DateFormat('dd/MM/yyyy').format(selectedDate);
    _valueController.text = widget.expense?.value.toStringAsFixed(2) ?? "0,00";
    _descriptionController.text = widget.expense?.description ?? "";
    selectedCategory = widget.expense?.category ?? ExpenseCategoryEnum.FOOD;
  }

  double getDoubleValueFromText(String text) {
    return double.parse(text.replaceAll(".", "").replaceAll(",", "."));
  }

  String getTextFromCategory(ExpenseCategoryEnum category) {
    switch (category) {
      case ExpenseCategoryEnum.FOOD:
        return "Alimentação";
      case ExpenseCategoryEnum.FUEL:
        return "Combustível";
      case ExpenseCategoryEnum.CAR_RENT:
        return "Aluguel de veículo";
      case ExpenseCategoryEnum.HOTEL:
        return "Hospedagem";
      default:
        return "Outros";
    }
  }

  @override
  void initState() {
    super.initState();
    //settingup date
    _setupData();
  }

  @override
  Widget build(BuildContext context) {
    return BasePageBody(
      shouldShowBackButton: true,
      shouldShowBottomAppBar: false,
      title: "Despesa",
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: BlocConsumer(
          listener: (context, state) {
            if (state is ExpenseFormInitial) {
              if(state.success){
                //display snack
                ScaffoldMessenger.of(context).showSnackBar(
                   SnackBar(
                    content: const Text("Despesa salva com sucesso", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                    backgroundColor: ColorUtils.successColor,
                  ),
                );

                //close screen
                Future.delayed(const Duration(seconds: 1), (){
                  Navigator.pop(context);
                });
              }else if(state.hasError){
                //display snack
                ScaffoldMessenger.of(context).showSnackBar(
                   SnackBar(
                    content: Text(state.errorMessage ?? "Erro ao salvar despesa", style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                    backgroundColor: ColorUtils.errorColor,
                  ),
                );
              }
            }
          },
          bloc: _bloc,
          builder: (context, state) {
            return SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      widget.expense != null
                          ? "Editar uma despesa"
                          : "Adicionar uma despesa",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Categoria",
                      style: TextStyle(
                          color: ColorUtils.borderColor, fontSize: 14),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    DropdownMenu(
                      inputDecorationTheme: InputDecorationTheme(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: ColorUtils.borderColor),
                        ),
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 15),
                      ),
                      menuStyle: MenuStyle(
                        backgroundColor: MaterialStateColor.resolveWith(
                            (states) => ColorUtils.whiteColor),
                      ),
                      width: MediaQuery.of(context).size.width - 40,
                      initialSelection: selectedCategory.name,
                      dropdownMenuEntries: ExpenseCategoryEnum.values
                          .map((e) => DropdownMenuEntry(
                              value: e.name, label: getTextFromCategory(e)))
                          .toList(),
                      onSelected: (value) {
                        setState(() {
                          selectedCategory = ExpenseCategoryEnum.values
                                  .where((element) => element.name == value)
                                  .firstOrNull ??
                              ExpenseCategoryEnum.OTHER;
                        });
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Data da Despesa",
                                style: TextStyle(
                                    color: ColorUtils.borderColor,
                                    fontSize: 14),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextFormField(
                                controller: _dateController,
                                readOnly: true,
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: const Icon(Icons.calendar_today,
                                        size: 20),
                                    onPressed: () async {
                                      final result = await showDatePicker(
                                        context: context,
                                        initialDate: selectedDate,
                                        firstDate: DateTime(2000),
                                        lastDate: DateTime.now(),
                                      );

                                      if (result != null) {
                                        _dateController.text =
                                            DateFormat('dd/MM/yyyy')
                                                .format(result);
                                        selectedDate = result;
                                      }
                                    },
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 15),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: ColorUtils.borderColor),
                                  ),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Informe a data";
                                  }
                                  return null;
                                },
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Valor",
                                style: TextStyle(
                                    color: ColorUtils.borderColor,
                                    fontSize: 14),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextFormField(
                                controller: _valueController,
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                  CustomMoneyFormatter()
                                ],
                                decoration: InputDecoration(
                                  prefix: const Text("R\$ ", style: TextStyle(fontSize: 12)),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 15),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: ColorUtils.borderColor),
                                  ),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Informe o valor";
                                  } else if (getDoubleValueFromText(value) ==
                                      0) {
                                    return "Valor não pode ser 0";
                                  }
                                  return null;
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Descrição",
                      style: TextStyle(
                          color: ColorUtils.borderColor, fontSize: 14),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      controller: _descriptionController,
                      maxLines: 4,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 15),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: ColorUtils.borderColor),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Dê uma descrição para a despesa";
                        }
                        return null;
                      },
                    ),
                    const SizedBox(
                      height: 80,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        backgroundColor: ColorUtils.successColor,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          if (widget.expense != null) {
                            //update
                            print(widget.expense?.id);
                            final newExpense = Expense(
                              id: widget.expense!.id,
                              description: _descriptionController.text,
                              value: getDoubleValueFromText(_valueController.text),
                              category: selectedCategory,
                              date: selectedDate,
                            );

                            _bloc.add(UpdateExpenseFormEvent(
                                newExpense: newExpense,
                                oldExpense: widget.expense!));
                          } else {
                            //add
                            _bloc.add(SaveExpenseFormEvent(
                                category: selectedCategory,
                                value: getDoubleValueFromText(_valueController.text),
                                description: _descriptionController.text,
                                date: selectedDate));
                          }
                        }
                      },
                      child: Text(
                        widget.expense != null ? "Editar" : "Adicionar",
                        style: const TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Center(
                        child: Text(
                          "Cancelar",
                          style: TextStyle(
                              fontSize: 14,
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
