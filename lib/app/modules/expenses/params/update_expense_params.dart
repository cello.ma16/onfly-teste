import '../../../core/domain/entities/expense.dart';

class UpdateExpenseParams{
  final Expense oldExpense;
  final Expense newExpense;

  UpdateExpenseParams({required this.oldExpense, required this.newExpense});
}