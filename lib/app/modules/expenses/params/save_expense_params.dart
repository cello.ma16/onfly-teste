import 'package:onfly_teste/app/core/domain/entities/expense.dart';

class SaveExpenseParams{
  final String description;
  final double value;
  final ExpenseCategoryEnum category;
  final DateTime date;

  SaveExpenseParams({required this.description, required this.value, required this.category, required this.date});

  Map<String, dynamic> toMap(){
    return {
      "description": description,
      "value": value,
      "category": category.name,
      "date": date.toIso8601String()
    };
  }
}