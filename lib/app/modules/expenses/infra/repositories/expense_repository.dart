import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/expenses/exceptions/expenses_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/expenses/infra/datasources/i_expense_datasource.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/types/expenses_types.dart';

import '../../domain/repositories/i_expenses_repository.dart';

class ExpenseRepository implements IExpensesRepository {
  final IExpenseDatasource expenseDatasource;

  ExpenseRepository({required this.expenseDatasource});

  @override
  Future<GetExpensesType> getExpenses() async {
    try {
      final result = await expenseDatasource.getExpenses();
      return Right(result);
    } catch (e) {
      print(e);
      return Left(ExpensesUnexpectedDomainException());
    }
  }

  @override
  Future<SaveExpenseType> saveExpense(
      {required SaveExpenseParams params}) async {
    try {
      await expenseDatasource.saveExpense(params: params);
      return const Right(true);
    } catch (e) {
      return Left(ExpensesUnexpectedDomainException());
    }
  }

  @override
  Future<UpdateExpenseType> updateExpense({required UpdateExpenseParams params}) async {
    try{
      await expenseDatasource.updateExpense(params: params);
      return const Right(true);
    } catch(e){
      return Left(ExpensesUnexpectedDomainException());
    }
  }
}
