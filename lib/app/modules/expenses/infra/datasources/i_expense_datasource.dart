import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';

abstract class IExpenseDatasource{
  Future<List<Expense>> getExpenses();
  Future<void> saveExpense({required SaveExpenseParams params});
  Future<void> updateExpense({required UpdateExpenseParams params});
}