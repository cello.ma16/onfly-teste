import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/infra/datasources/i_expense_datasource.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';

class ExpenseDatasource implements IExpenseDatasource{

  final IExpenseDao expenseDao;

  ExpenseDatasource({required this.expenseDao});

  @override
  Future<List<Expense>> getExpenses() async {
    final result = await expenseDao.getExpenses();

    return result.map((e) => Expense.fromMap(e)).toList();
  }

  @override
  Future<void> saveExpense({required SaveExpenseParams params}) async {
    return await expenseDao.saveExpense(params: params.toMap());
  }

  @override
  Future<void> updateExpense({required UpdateExpenseParams params}) async {
    await expenseDao.updateExpense(params: params.newExpense.toMap());
  }
}