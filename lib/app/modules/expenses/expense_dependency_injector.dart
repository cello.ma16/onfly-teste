import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/get_expenses_usecase.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/save_expense_usecase.dart';
import 'package:onfly_teste/app/modules/expenses/infra/datasources/i_expense_datasource.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_form/bloc/expense_form_bloc.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_list/bloc/expenses_bloc.dart';

import '../../core/dependencies/i_dependency_injector.dart';
import 'domain/usecases/update_expense_usecase.dart';
import 'external/datasources/expense_datasource.dart';
import 'infra/repositories/expense_repository.dart';

class ExpenseDependencyInjector implements IDependencyInjector {
  final _getIt = GetIt.instance;

  @override
  void init() {
    //DATASOURCE
    _getIt.registerLazySingleton<IExpenseDatasource>(
        () => ExpenseDatasource(expenseDao: _getIt.get<IExpenseDao>()));

    //REPOSITORIES
    _getIt.registerLazySingleton<IExpensesRepository>(() =>
        ExpenseRepository(expenseDatasource: _getIt.get<IExpenseDatasource>()));

    //USECASES
    _getIt.registerLazySingleton<IGetExpensesUsecase>(() => GetExpensesUsecase(
        expensesRepository: _getIt.get<IExpensesRepository>()));
    _getIt.registerLazySingleton<ISaveExpenseUsecase>(() => SaveExpenseUsecase(
        expensesRepository: _getIt.get<IExpensesRepository>()));
    _getIt.registerLazySingleton<IUpdateExpenseUsecase>(() => UpdateExpenseUsecase(
        expensesRepository: _getIt.get<IExpensesRepository>()));

    //BLOC
    _getIt.registerFactory<ExpensesBloc>(() =>
        ExpensesBloc(getExpensesUsecase: _getIt.get<IGetExpensesUsecase>()));
    _getIt.registerFactory<ExpenseFormBloc>(() =>
        ExpenseFormBloc(
            updateExpenseUsecase: _getIt.get<IUpdateExpenseUsecase>(),
            saveExpenseUsecase: _getIt.get<ISaveExpenseUsecase>()));
  }
}
