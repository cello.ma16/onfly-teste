import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/exceptions/expenses_domain_exceptions.dart';

typedef GetExpensesType = Either<ExpensesDomainException, List<Expense>>;
typedef SaveExpenseType = Either<ExpensesDomainException, bool>;
typedef UpdateExpenseType = Either<ExpensesDomainException, bool>;