abstract class ExpensesDomainException implements Exception {
  final String message;
  ExpensesDomainException(this.message);
}

class ExpensesUnexpectedDomainException extends ExpensesDomainException {
  ExpensesUnexpectedDomainException() : super("Um erro inesperado ocorreu. Por favor, tente novamente mais tarde");
}

class ExpensesInvalidParamsDomainException extends ExpensesDomainException {
  ExpensesInvalidParamsDomainException() : super("Parâmetros inválidos");
}

class NotChangedExpenseDomainException extends ExpensesDomainException {
  NotChangedExpenseDomainException() : super("Nenhuma alteração foi feita");
}

class NotSameExpenseDomainException extends ExpensesDomainException {
  NotSameExpenseDomainException() : super("As despesas não são as mesmas");
}


