import '../../params/save_expense_params.dart';
import '../../params/update_expense_params.dart';
import '../../types/expenses_types.dart';

abstract class IExpensesRepository {
  Future<GetExpensesType> getExpenses();
  Future<SaveExpenseType> saveExpense({required SaveExpenseParams params});
  Future<UpdateExpenseType> updateExpense({required UpdateExpenseParams params});
}