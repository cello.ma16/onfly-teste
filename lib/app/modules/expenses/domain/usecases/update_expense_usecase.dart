import 'package:dartz/dartz.dart';

import '../../exceptions/expenses_domain_exceptions.dart';
import '../../params/update_expense_params.dart';
import '../../types/expenses_types.dart';
import '../repositories/i_expenses_repository.dart';

abstract class IUpdateExpenseUsecase {
  Future<UpdateExpenseType> call({required UpdateExpenseParams params});
}

class UpdateExpenseUsecase implements IUpdateExpenseUsecase {
  final IExpensesRepository expensesRepository;

  UpdateExpenseUsecase({required this.expensesRepository});

  @override
  Future<UpdateExpenseType> call({required UpdateExpenseParams params}) async {

    if(params.oldExpense == params.newExpense){
      return Left(NotChangedExpenseDomainException());
    }

    if(params.oldExpense.id != params.newExpense.id){
      return Left(NotSameExpenseDomainException());
    }

    return await expensesRepository.updateExpense(params: params);
  }
}