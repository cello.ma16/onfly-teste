import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/types/expenses_types.dart';

import '../../exceptions/expenses_domain_exceptions.dart';

abstract class ISaveExpenseUsecase {
  Future<SaveExpenseType> call({required SaveExpenseParams params});
}

class SaveExpenseUsecase implements ISaveExpenseUsecase {

  final IExpensesRepository expensesRepository;

  SaveExpenseUsecase({required this.expensesRepository});

  @override
  Future<SaveExpenseType> call({required SaveExpenseParams params}) async {

    if(params.value <= 0 || params.description.isEmpty){
      return Left(ExpensesInvalidParamsDomainException());
    }

    return await expensesRepository.saveExpense(params: params);
  }
}