import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';

import '../../types/expenses_types.dart';

abstract class IGetExpensesUsecase{
  Future<GetExpensesType> call();
}

class GetExpensesUsecase implements IGetExpensesUsecase{

  final IExpensesRepository expensesRepository;

  GetExpensesUsecase({required this.expensesRepository});

  @override
  Future<GetExpensesType> call() async {
    return await expensesRepository.getExpenses();
  }
}