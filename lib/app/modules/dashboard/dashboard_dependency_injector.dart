import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/dependencies/i_dependency_injector.dart';
import 'package:onfly_teste/app/modules/dashboard/domain/usecases/get_expenses_between_dates_usecase.dart';
import 'package:onfly_teste/app/modules/dashboard/external/datasources/dashboard_expenses_datasource.dart';
import 'package:onfly_teste/app/modules/dashboard/infra/datasources/i_dashboard_expenses_datasource.dart';
import 'package:onfly_teste/app/modules/dashboard/presentation/pages/dashboard/bloc/dashboard_bloc.dart';

import 'domain/repositories/dashboard_expenses_repository.dart';
import 'infra/repositories/dashboard_expenses_repository.dart';

class DashboardDependencyInjector implements IDependencyInjector {
  final _getIt = GetIt.instance;

  @override
  void init() {
    //Datasources
    _getIt.registerLazySingleton<IDashboardExpensesDatasource>(
        () => DashboardExpensesDatasource(expenseDao: _getIt.get()));
    //Repositories
    _getIt.registerLazySingleton<IDashboardExpensesRepository>(() =>
        DashboardExpensesRepository(dashboardExpensesDatasource: _getIt.get()));
    //Usecases
    _getIt.registerLazySingleton<IGetExpensesBetweenDatesUsecase>(() =>
        GetExpensesBetweenDatesUsecase(
            dashboardExpensesRepository: _getIt.get()));
    //Bloc
    _getIt.registerLazySingleton<DashboardBloc>(
        () => DashboardBloc(getExpensesBetweenDatesUsecase: _getIt.get()));
  }
}
