import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';

import '../exceptions/dashboard_domain_exceptions.dart';

typedef GetExpensesBetweenDatesType = Either<DashboardDomainException, List<Expense>>;