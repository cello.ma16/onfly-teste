class GetExpensesBetweenDatesParams {
  final DateTime startDate;
  final DateTime endDate;

  GetExpensesBetweenDatesParams({
    required this.startDate,
    required this.endDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'startDate': startDate.toIso8601String(),
      'endDate': endDate.toIso8601String(),
    };
  }
}