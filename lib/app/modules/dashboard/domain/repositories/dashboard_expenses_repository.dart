import 'package:onfly_teste/app/modules/dashboard/types/dashboard_types.dart';

import '../../params/get_expenses_between_dates_params.dart';

abstract class IDashboardExpensesRepository {
  Future<GetExpensesBetweenDatesType> getExpensesBetweenDates(
      {required GetExpensesBetweenDatesParams params});
}