import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/dashboard/domain/repositories/dashboard_expenses_repository.dart';

import '../../exceptions/dashboard_domain_exceptions.dart';
import '../../params/get_expenses_between_dates_params.dart';
import '../../types/dashboard_types.dart';

abstract class IGetExpensesBetweenDatesUsecase {
  Future<GetExpensesBetweenDatesType> call(
      {required GetExpensesBetweenDatesParams params});
}

class GetExpensesBetweenDatesUsecase implements IGetExpensesBetweenDatesUsecase {
  final IDashboardExpensesRepository dashboardExpensesRepository;

  GetExpensesBetweenDatesUsecase({required this.dashboardExpensesRepository});

  @override
  Future<GetExpensesBetweenDatesType> call(
      {required GetExpensesBetweenDatesParams params}) async {

    if(!params.startDate.isBefore(params.endDate)){
      return Left(DashboardWrongDatesDomainException());
    }

    return await dashboardExpensesRepository.getExpensesBetweenDates(params: params);
  }
}