import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:onfly_teste/app/core/presentation/widgets/base_page_body.dart';
import 'package:onfly_teste/app/modules/dashboard/presentation/pages/dashboard/bloc/dashboard_event.dart';
import 'package:onfly_teste/app/modules/dashboard/presentation/pages/dashboard/bloc/dashboard_state.dart';

import '../../../../../core/domain/entities/expense.dart';
import '../../../../../core/utils/color_utils.dart';
import 'bloc/dashboard_bloc.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final _bloc = GetIt.instance.get<DashboardBloc>();

  DateTime _startDate = DateTime.now().subtract(const Duration(days: 30));
  DateTime _endDate = DateTime.now();

  List<Expense> getFoodExpense(List<Expense> expenses) {
    return expenses
        .where((element) => element.category == ExpenseCategoryEnum.FOOD)
        .toList();
  }

  List<Expense> getHotelExpense(List<Expense> expenses) {
    return expenses
        .where((element) => element.category == ExpenseCategoryEnum.HOTEL)
        .toList();
  }

  List<Expense> getFuelExpense(List<Expense> expenses) {
    return expenses
        .where((element) => element.category == ExpenseCategoryEnum.FUEL)
        .toList();
  }

  List<Expense> getCarRentExpense(List<Expense> expenses) {
    return expenses
        .where((element) => element.category == ExpenseCategoryEnum.CAR_RENT)
        .toList();
  }

  List<Expense> getOtherExpense(List<Expense> expenses) {
    return expenses
        .where((element) => element.category == ExpenseCategoryEnum.OTHER)
        .toList();
  }

  @override
  void initState() {
    super.initState();
    _bloc.add(DashboardGetExpensesEvent(
        startDate: DateTime.now().subtract(const Duration(days: 30)),
        endDate: DateTime.now()));
  }

  @override
  Widget build(BuildContext context) {
    return BasePageBody(
      title: "Dashboard",
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: BlocBuilder(
            bloc: _bloc,
            builder: (context, state) {
              if (state is DashboardLoadedState) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text(
                      "Dados das suas despesas",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Início do Periodo",
                                style: TextStyle(
                                    color: ColorUtils.borderColor, fontSize: 14),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextFormField(
                                controller: TextEditingController(
                                    text: DateFormat('dd/MM/yyyy')
                                        .format(_startDate)),
                                readOnly: true,
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: const Icon(Icons.calendar_today,
                                        size: 20),
                                    onPressed: () async {
                                      final result = await showDatePicker(
                                        context: context,
                                        initialDate: _startDate,
                                        firstDate: DateTime(2000),
                                        lastDate: _endDate,
                                      );
        
                                      if (result != null) {
                                        setState(() {
                                          _startDate = result;
                                        });
        
                                        _bloc.add(DashboardGetExpensesEvent(
                                            startDate: _startDate,
                                            endDate: _endDate));
                                      }
                                    },
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 15),
                                  border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: ColorUtils.borderColor),
                                  ),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Informe a data";
                                  }
                                  return null;
                                },
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Fim do Período",
                                style: TextStyle(
                                    color: ColorUtils.borderColor, fontSize: 14),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextFormField(
                                controller: TextEditingController(
                                    text: DateFormat('dd/MM/yyyy')
                                        .format(_endDate)),
                                readOnly: true,
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: const Icon(Icons.calendar_today,
                                        size: 20),
                                    onPressed: () async {
                                      final result = await showDatePicker(
                                        context: context,
                                        initialDate: _endDate,
                                        firstDate: _startDate,
                                        lastDate: DateTime.now(),
                                      );
        
                                      if (result != null) {
                                        setState(() {
                                          _endDate = result;
                                        });
        
                                        _bloc.add(DashboardGetExpensesEvent(
                                            startDate: _startDate,
                                            endDate: _endDate));
                                      }
                                    },
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 15),
                                  border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: ColorUtils.borderColor),
                                  ),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Informe a data";
                                  }
                                  return null;
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    SizedBox(
                      height: 200,
                      child: state.expenses.isEmpty ? Center(
                        child: Text("Nenhuma despesa encontrada"),
                      ) : PieChart(
                        PieChartData(
                          borderData: FlBorderData(
                            show: false,
                          ),
                          sectionsSpace: 0,
                          centerSpaceRadius: 0,
                          sections: [
                            PieChartSectionData(
                              color: Colors.red,
                              value: getFoodExpense(state.expenses).length /
                                  state.expenses.length *
                                  100,
                              title: "Alimentação",
                              titleStyle: const TextStyle(color: Colors.white),
                              radius: 100,
                            ),
                            PieChartSectionData(
                              color: Colors.blue,
                              value: getHotelExpense(state.expenses).length /
                                  state.expenses.length *
                                  100,
                              titleStyle: const TextStyle(color: Colors.white),
                              title: "Hotel",
                              radius: 100,
                            ),
                            PieChartSectionData(
                              color: Colors.green,
                              value: getFuelExpense(state.expenses).length /
                                  state.expenses.length *
                                  100,
                              title: "Combustível",
                              titleStyle: const TextStyle(color: Colors.white),
                              radius: 100,
                            ),
                            PieChartSectionData(
                              color: Colors.yellow,
                              value: getCarRentExpense(state.expenses).length /
                                  state.expenses.length *
                                  100,
                              title: "Aluguel de Carro",
                              titleStyle: const TextStyle(color: Colors.white),
                              radius: 100,
                            ),
                            PieChartSectionData(
                              color: Colors.purple,
                              value: getOtherExpense(state.expenses).length /
                                  state.expenses.length *
                                  100,
                              title: "Outros",
                              titleStyle: const TextStyle(color: Colors.white),
                              radius: 100,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    const Text(
                      "Legenda",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    _buildSubtitle(),
                  ],
                );
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Widget _buildSubtitle() {
    return Column(
      children: [
        Row(
          children: [
            _buildSubtitleItem(
              color: Colors.red,
              title: "Alimentação",
            ),
            const SizedBox(
              width: 20,
            ),
            _buildSubtitleItem(
              color: Colors.blue,
              title: "Hotel",
            ),
          ],
        ),
        Row(
          children: [
            _buildSubtitleItem(
              color: Colors.green,
              title: "Combustível",
            ),
            const SizedBox(
              width: 20,
            ),
            _buildSubtitleItem(
              color: Colors.yellow,
              title: "Aluguel de Carro",
            ),
          ],
        ),
        _buildSubtitleItem(
          color: Colors.purple,
          title: "Outros",
        ),
      ],
    );
  }

  Widget _buildSubtitleItem({required Color color, required String title}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          width: 20,
          height: 20,
          color: color,
        ),
        const SizedBox(
          width: 5,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
