import 'package:equatable/equatable.dart';

import '../../../../../../core/domain/entities/expense.dart';

abstract class DashboardState extends Equatable {}

class DashboardInitialState extends DashboardState {
  @override
  List<Object?> get props => [];
}

class DashboardLoadingState extends DashboardState {
  @override
  List<Object?> get props => [];
}

class DashboardLoadedState extends DashboardState {

  final List<Expense> expenses;

  DashboardLoadedState({required this.expenses});

  DashboardLoadedState copyWith({List<Expense>? expenses}) {
    return DashboardLoadedState(
      expenses: expenses ?? this.expenses
    );
  }

  @override
  List<Object?> get props => [
    expenses
  ];
}