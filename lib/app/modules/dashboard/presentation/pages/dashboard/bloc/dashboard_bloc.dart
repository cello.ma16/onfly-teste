import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/dashboard/domain/usecases/get_expenses_between_dates_usecase.dart';
import 'package:onfly_teste/app/modules/dashboard/params/get_expenses_between_dates_params.dart';

import 'dashboard_event.dart';
import 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  final IGetExpensesBetweenDatesUsecase getExpensesBetweenDatesUsecase;

  DashboardBloc({required this.getExpensesBetweenDatesUsecase}) : super(DashboardInitialState()){
    on<DashboardGetExpensesEvent>(_onGetExpenses);
  }

  Future<void> _onGetExpenses(DashboardGetExpensesEvent event, Emitter<DashboardState> emit) async {
    emit(DashboardLoadingState());

    final params = GetExpensesBetweenDatesParams(startDate: event.startDate, endDate: event.endDate);

    final result = await getExpensesBetweenDatesUsecase(params: params);
    result.fold(
      (failure) {
        emit(DashboardLoadedState(expenses: const <Expense>[]));
      },
      (expenses) {
        emit(DashboardLoadedState(expenses: expenses));
      },
    );
  }
}