abstract class DashboardEvent{}

class DashboardGetExpensesEvent extends DashboardEvent{
  final DateTime startDate;
  final DateTime endDate;

  DashboardGetExpensesEvent({required this.startDate, required this.endDate});
}