abstract class DashboardDomainException implements Exception {
  final String message;

  DashboardDomainException(this.message);
}

class DashboardUnexpectedDomainException extends DashboardDomainException {
  DashboardUnexpectedDomainException()
      : super(
            "Um erro inesperado ocorreu. Por favor, tente novamente mais tarde");
}

class DashboardWrongDatesDomainException extends DashboardDomainException {
  DashboardWrongDatesDomainException()
      : super("A data inicial deve ser anterior à data final");
}

