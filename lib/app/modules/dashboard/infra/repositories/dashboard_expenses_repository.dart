import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/dashboard/exceptions/dashboard_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/dashboard/params/get_expenses_between_dates_params.dart';
import 'package:onfly_teste/app/modules/dashboard/types/dashboard_types.dart';
import '../../domain/repositories/dashboard_expenses_repository.dart';
import '../datasources/i_dashboard_expenses_datasource.dart';

class DashboardExpensesRepository implements IDashboardExpensesRepository {
  final IDashboardExpensesDatasource dashboardExpensesDatasource;

  DashboardExpensesRepository({required this.dashboardExpensesDatasource});

  @override
  Future<GetExpensesBetweenDatesType> getExpensesBetweenDates(
      {required GetExpensesBetweenDatesParams params}) async {
    try {
      final expenses = await dashboardExpensesDatasource
          .getExpensesBetweenDates(params: params);
      return Right(expenses);
    } catch (e) {
      return Left(DashboardUnexpectedDomainException());
    }
  }
}
