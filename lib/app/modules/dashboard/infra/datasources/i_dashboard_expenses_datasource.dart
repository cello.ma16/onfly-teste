import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/dashboard/params/get_expenses_between_dates_params.dart';

abstract class IDashboardExpensesDatasource {
  Future<List<Expense>> getExpensesBetweenDates({required GetExpensesBetweenDatesParams params });
}