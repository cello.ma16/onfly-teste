import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:onfly_teste/app/modules/dashboard/infra/datasources/i_dashboard_expenses_datasource.dart';

import '../../../../core/domain/entities/expense.dart';
import '../../params/get_expenses_between_dates_params.dart';

class DashboardExpensesDatasource implements IDashboardExpensesDatasource {
  final IExpenseDao expenseDao;

  DashboardExpensesDatasource({required this.expenseDao});

  @override
  Future<List<Expense>> getExpensesBetweenDates(
      {required GetExpensesBetweenDatesParams params}) async {
    final result = await expenseDao.getExpensesBetweenDates(
        startDate: params.startDate.subtract(const Duration(seconds: 1)),
        endDate: params.endDate.add(const Duration(seconds: 1)));

    return result.map((e) => Expense.fromMap(e)).toList();
  }
}
