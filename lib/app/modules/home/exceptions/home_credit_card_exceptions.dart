class HomeCreditCardDomainException implements Exception {
  final String message;

  HomeCreditCardDomainException(this.message);
}

class HomeCreditCardUnexpectedException extends HomeCreditCardDomainException {
  HomeCreditCardUnexpectedException() : super('Um erro inesperado ocorreu, tente novamente mais tarde');
}