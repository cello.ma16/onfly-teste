abstract class HomeTravelDomainException implements Exception {
  final String message;

  HomeTravelDomainException(this.message);
}

class HomeTravelUnexpectedException extends HomeTravelDomainException {
  HomeTravelUnexpectedException() : super("Um erro inesperado ocorreu, tente novamente mais tarde");
}