abstract class HomeExpenseDomainExceptions implements Exception {
  final String message;

  HomeExpenseDomainExceptions({required this.message});
}

class HomeExpenseUnexpectedException extends HomeExpenseDomainExceptions {
  HomeExpenseUnexpectedException() : super(message: 'Um erro inesperado ocorreu, tente novamente mais tarde');
}