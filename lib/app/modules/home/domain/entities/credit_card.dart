enum CreditCardFlagEnum { VISA, MASTERCARD }

class CreditCard{
  final int id;
  final String number;
  final String name;
  final CreditCardFlagEnum flag;
  final double limit;

  CreditCard({
    required this.id,
    required this.number,
    required this.name,
    required this.flag,
    required this.limit
  });

  @override
  String toString() {
    return 'CreditCard{id: $id, number: $number, name: $name, flag: $flag, limit: $limit}';
  }

  factory CreditCard.fromMap(Map<String, dynamic> map) {
    return CreditCard(
      id: map['id'],
      number: map['number'],
      name: map['name'],
      flag: CreditCardFlagEnum.values
        .where((element) => element.name == map['flag'])
        .firstOrNull ?? CreditCardFlagEnum.MASTERCARD,
      limit: map['limit']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'number': number,
      'name': name,
      'flag': flag.toString(),
      'limit': limit
    };
  }
}