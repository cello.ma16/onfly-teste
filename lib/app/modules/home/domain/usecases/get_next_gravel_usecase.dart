import 'package:onfly_teste/app/modules/home/domain/repositories/i_home_travel_repository.dart';
import 'package:onfly_teste/app/modules/home/types/home_types.dart';

abstract class IGetNextTravelUsecase {
  Future<GetNextTravelType> call();
}

class GetNextTravelUsecase implements IGetNextTravelUsecase {

  final IHomeTravelRepository homeTravelRepository;

  GetNextTravelUsecase({required this.homeTravelRepository});

  @override
  Future<GetNextTravelType> call() async {
    return await homeTravelRepository.getNextTravel();
  }
}