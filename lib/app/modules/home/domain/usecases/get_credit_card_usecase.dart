import 'package:onfly_teste/app/modules/home/domain/repositories/i_credit_card_repository.dart';
import 'package:onfly_teste/app/modules/home/types/home_types.dart';

abstract class IGetCreditCardUsecase {
  Future<GetCreditCardType> call();
}

class GetCreditCardUsecase implements IGetCreditCardUsecase {

  final ICreditCardRepository creditCardRepository;

  GetCreditCardUsecase({required this.creditCardRepository});

  @override
  Future<GetCreditCardType> call() async {
    return await creditCardRepository.getCreditCard();
  }
}