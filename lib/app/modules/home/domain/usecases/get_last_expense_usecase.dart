import '../../types/home_types.dart';
import '../repositories/i_home_expense_repository.dart';

abstract class IGetLastExpenseUsecase {
  Future<GetLastExpenseType> call();
}

class GetLastExpenseUsecase implements IGetLastExpenseUsecase {

  IHomeExpenseRepository homeExpenseRepository;

  GetLastExpenseUsecase({required this.homeExpenseRepository});

  @override
  Future<GetLastExpenseType> call() async {
    return await homeExpenseRepository.getLastExpense();
  }
}