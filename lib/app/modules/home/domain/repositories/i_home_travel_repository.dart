import 'package:onfly_teste/app/modules/home/types/home_types.dart';

abstract class IHomeTravelRepository {
  Future<GetNextTravelType> getNextTravel();
}

