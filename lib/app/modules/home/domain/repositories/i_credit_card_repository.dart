import 'package:onfly_teste/app/modules/home/types/home_types.dart';

abstract class ICreditCardRepository{
  Future<GetCreditCardType> getCreditCard();
}