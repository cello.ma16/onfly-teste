import 'package:onfly_teste/app/core/domain/entities/expense.dart';

import '../../../../core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import '../../infra/datasources/i_home_expense_datasource.dart';

class HomeExpenseDatasource implements IHomeExpenseDatasource {

  final IExpenseDao expenseDao;

  HomeExpenseDatasource({required this.expenseDao});

  @override
  Future<Expense?> getLastExpense() async {
    final result = await expenseDao.getLastExpense();

    if(result == null) return null;

    return Expense.fromMap(result);
  }

}