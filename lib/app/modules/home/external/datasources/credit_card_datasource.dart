import 'package:onfly_teste/app/core/services/database/sqlite/daos/credit_card/i_credit_card_dao.dart';
import 'package:onfly_teste/app/modules/home/domain/entities/credit_card.dart';

import '../../infra/datasources/i_credit_card_datasource.dart';

class CreditCardDatasource implements ICreditCardDatasource {

  final ICreditCardDao creditCardDao;

  CreditCardDatasource({required this.creditCardDao});

  @override
  Future<CreditCard?> getCreditCard() async {
    final result = await creditCardDao.getCreditCard();
    if (result == null) return null;
    return CreditCard.fromMap(result);
  }
}