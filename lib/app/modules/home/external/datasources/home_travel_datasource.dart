import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/i_travels_dao.dart';

import '../../../../core/domain/entities/travel.dart';
import '../../infra/datasources/i_home_travel_datasource.dart';

class HomeTravelDatasource implements IHomeTravelDatasource {

  final ITravelsDao travelsDao;

  HomeTravelDatasource({required this.travelsDao});

  @override
  Future<Travel?> getNextTravel() async {
    final result = await travelsDao.getNextTravel();

    if(result == null) return null;

    return Travel.fromJson(result);
  }
}