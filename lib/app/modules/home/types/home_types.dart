import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/home/exceptions/home_credit_card_exceptions.dart';
import 'package:onfly_teste/app/modules/home/exceptions/home_expense_exceptions.dart';
import 'package:onfly_teste/app/modules/home/exceptions/home_travel_exceptions.dart';

import '../../../core/domain/entities/expense.dart';
import '../../../core/domain/entities/travel.dart';
import '../domain/entities/credit_card.dart';

typedef GetCreditCardType = Either<HomeCreditCardDomainException, CreditCard?>;

typedef GetNextTravelType = Either<HomeTravelDomainException, Travel?>;

typedef GetLastExpenseType = Either<HomeExpenseDomainExceptions, Expense?>;