import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/dependencies/i_dependency_injector.dart';
import 'package:onfly_teste/app/modules/home/domain/usecases/get_credit_card_usecase.dart';
import 'package:onfly_teste/app/modules/home/domain/usecases/get_last_expense_usecase.dart';
import 'package:onfly_teste/app/modules/home/domain/usecases/get_next_gravel_usecase.dart';
import 'package:onfly_teste/app/modules/home/external/datasources/credit_card_datasource.dart';
import 'package:onfly_teste/app/modules/home/infra/datasources/i_home_expense_datasource.dart';
import 'package:onfly_teste/app/modules/home/infra/datasources/i_home_travel_datasource.dart';
import 'package:onfly_teste/app/modules/home/presentation/pages/home/bloc/home_bloc.dart';

import 'domain/repositories/i_credit_card_repository.dart';
import 'domain/repositories/i_home_expense_repository.dart';
import 'domain/repositories/i_home_travel_repository.dart';
import 'external/datasources/home_expense_datasource.dart';
import 'external/datasources/home_travel_datasource.dart';
import 'infra/datasources/i_credit_card_datasource.dart';
import 'infra/repositories/credit_card_repository.dart';
import 'infra/repositories/home_expense_repository.dart';
import 'infra/repositories/home_travel_repository.dart';

class HomeDependencyInjector implements IDependencyInjector {
  final _getIt = GetIt.instance;

  @override
  void init() {
    //Datasources
    _getIt.registerLazySingleton<ICreditCardDatasource>(
        () => CreditCardDatasource(creditCardDao: _getIt.get()));
    _getIt.registerLazySingleton<IHomeExpenseDatasource>(
        () => HomeExpenseDatasource(expenseDao: _getIt.get()));
    _getIt.registerLazySingleton<IHomeTravelDatasource>(
        () => HomeTravelDatasource(travelsDao: _getIt.get()));
    //Repositories
    _getIt.registerLazySingleton<ICreditCardRepository>(
        () => CreditCardRepository(creditCardDatasource: _getIt.get()));
    _getIt.registerLazySingleton<IHomeExpenseRepository>(
        () => HomeExpenseRepository(homeExpenseDatasource: _getIt.get()));
    _getIt.registerLazySingleton<IHomeTravelRepository>(
        () => HomeTravelRepository(homeTravelDatasource: _getIt.get()));
    //Usecases
    _getIt.registerLazySingleton<IGetCreditCardUsecase>(
        () => GetCreditCardUsecase(creditCardRepository: _getIt.get()));
    _getIt.registerLazySingleton<IGetLastExpenseUsecase>(
        () => GetLastExpenseUsecase(homeExpenseRepository: _getIt.get()));
    _getIt.registerLazySingleton<IGetNextTravelUsecase>(
        () => GetNextTravelUsecase(homeTravelRepository: _getIt.get()));
    //Bloc
    _getIt.registerLazySingleton<HomeBloc>(() => HomeBloc(
        getCreditCard: _getIt.get(),
        getLastExpenses: _getIt.get(),
        getNextTravel: _getIt.get()));
  }
}
