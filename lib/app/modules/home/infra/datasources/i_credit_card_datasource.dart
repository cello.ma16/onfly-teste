import '../../domain/entities/credit_card.dart';

abstract class ICreditCardDatasource {
  Future<CreditCard?> getCreditCard();
}