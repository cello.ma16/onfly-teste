import '../../../../core/domain/entities/travel.dart';

abstract class IHomeTravelDatasource {
  Future<Travel?> getNextTravel();
}