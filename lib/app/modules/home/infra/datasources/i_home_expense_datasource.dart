import 'package:onfly_teste/app/core/domain/entities/expense.dart';

abstract class IHomeExpenseDatasource {
  Future<Expense?> getLastExpense();
}