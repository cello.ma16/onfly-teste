import 'package:dartz/dartz.dart';

import '../../domain/repositories/i_home_expense_repository.dart';
import '../../exceptions/home_expense_exceptions.dart';
import '../../types/home_types.dart';
import '../datasources/i_home_expense_datasource.dart';

class HomeExpenseRepository implements IHomeExpenseRepository {

  final IHomeExpenseDatasource homeExpenseDatasource;

  HomeExpenseRepository({required this.homeExpenseDatasource});

  @override
  Future<GetLastExpenseType> getLastExpense() async {
    try{
      final result = await homeExpenseDatasource.getLastExpense();
      return Right(result);
    } catch(e){
      return Left(HomeExpenseUnexpectedException());
    }
  }
}