import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/home/exceptions/home_credit_card_exceptions.dart';
import 'package:onfly_teste/app/modules/home/infra/datasources/i_credit_card_datasource.dart';
import 'package:onfly_teste/app/modules/home/types/home_types.dart';

import '../../domain/repositories/i_credit_card_repository.dart';

class CreditCardRepository implements ICreditCardRepository {

  final ICreditCardDatasource creditCardDatasource;

  CreditCardRepository({required this.creditCardDatasource});

  @override
  Future<GetCreditCardType> getCreditCard() async {
    try{
      final result = await creditCardDatasource.getCreditCard();
      return Right(result);
    } catch(e){
      return Left(HomeCreditCardUnexpectedException());
    }
  }

}