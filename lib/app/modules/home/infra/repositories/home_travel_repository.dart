import 'package:dartz/dartz.dart';

import '../../domain/repositories/i_home_travel_repository.dart';
import '../../exceptions/home_travel_exceptions.dart';
import '../../types/home_types.dart';
import '../datasources/i_home_travel_datasource.dart';

class HomeTravelRepository implements IHomeTravelRepository {

  final IHomeTravelDatasource homeTravelDatasource;

  HomeTravelRepository({required this.homeTravelDatasource});

  @override
  Future<GetNextTravelType> getNextTravel() async {
    try{
      final result = await homeTravelDatasource.getNextTravel();
      return Right(result);
    } catch(e){
      return Left(HomeTravelUnexpectedException());
    }
  }
}