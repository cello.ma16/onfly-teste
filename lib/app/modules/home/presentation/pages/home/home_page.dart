import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/presentation/widgets/bottom_bar/bottom_bar.dart';
import 'package:onfly_teste/app/core/presentation/widgets/travel_card.dart';
import 'package:onfly_teste/app/core/utils/color_utils.dart';
import 'package:onfly_teste/app/core/utils/image_utils.dart';
import 'package:onfly_teste/app/modules/home/domain/entities/credit_card.dart';
import 'package:onfly_teste/app/modules/home/presentation/pages/home/bloc/home_bloc.dart';
import 'package:onfly_teste/app/modules/home/presentation/pages/home/bloc/home_state.dart';

import '../../../../../core/presentation/widgets/expense_card.dart';
import '../../../../../core/services/auth_provider/auth_provider.dart';
import '../../../../../core/services/bottom_app_provider/bottom_app_provider.dart';
import 'bloc/home_event.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _bloc = GetIt.instance.get<HomeBloc>();
  final _authProvider = GetIt.instance.get<IAuthProvider>();
  final _bottomAppProvider = GetIt.instance.get<IBottomAppProvider>();

  @override
  void initState() {
    _bloc.add(FetchAllHomeEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (context, state) {
          if (state is HomeLoadedState) {
            return Scaffold(
              bottomNavigationBar: BottomBar(),
              drawer: Drawer(
                child: Column(
                  children: [
                    const SizedBox(height: 50),
                    Image.asset(ImageUtils.logoColored, width: 200),
                    const Spacer(),
                    ListTile(
                      title: const Row(
                        children: [
                          Icon(Icons.exit_to_app),
                          SizedBox(width: 10),
                          Text("Sair"),
                        ],
                      ),
                      onTap: () {
                        _authProvider.removeUser();
                      },
                    ),
                  ],
                ),
              ),
              appBar: PreferredSize(
                preferredSize: Size(MediaQuery.of(context).size.width, 230),
                child: Container(
                  clipBehavior: Clip.none,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    ),
                  ),
                  child: OverflowBox(
                    alignment: Alignment.topCenter,
                    maxWidth: MediaQuery.of(context).size.width - 40,
                    maxHeight: double.infinity,
                    child: Builder(
                      builder: (context) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 30),
                            Row(
                              children: [
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: IconButton(
                                      onPressed: () {
                                        Scaffold.of(context).openDrawer();
                                      },
                                      icon: const Icon(Icons.menu),
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Image.asset(
                                    ImageUtils.logoWhite,
                                    width: 100,
                                  ),
                                ),
                                Spacer()
                              ],
                            ),
                            const SizedBox(height: 20),
                            const Text(
                              "Olá, Marcelo Luciano!",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            const Text(
                              "Preparamos um resumo de suas informações.",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                              ),
                            ),
                            const SizedBox(height: 20),
                            _buildCard(state.creditCard)
                          ],
                        );
                      }
                    ),
                  ),
                ),
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 100),
                      const Text("Próxima viagem",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                      const SizedBox(height: 10),
                      state.travel != null
                          ? TravelCard(travel: state.travel!)
                          : const SizedBox(
                              height: 105,
                              child:
                                  Center(child: Text("Sem viagens agendadas"))),
                      const SizedBox(height: 20),
                      const Text("Última despesa",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                      const SizedBox(height: 10),
                      state.expense != null
                          ? ExpenseCard(expense: state.expense!)
                          : const SizedBox(
                              height: 105,
                              child: Center(
                                  child: Text("Sem despesas cadastradas"))),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        });
  }

  Widget _buildCard(CreditCard? creditCard) {
    return Container(
      padding: const EdgeInsets.all(12),
      height: 160,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: creditCard == null ?
      const Center(child: Text("Você ainda não possui o Azulzinho"))
      : Column(
        children: [
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Azulzinho",
                    style: TextStyle(
                        color: ColorUtils.primaryColor,
                        fontSize: 14,
                        fontWeight: FontWeight.bold)),
                Image.asset(ImageUtils.logoColored, width: 80)
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Text(
                      "Saldo",
                      style:
                          TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "R\$ ${creditCard.limit.toStringAsFixed(2)}",
                      style: TextStyle(
                          color: ColorUtils.fontBlackColor,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("**** ${creditCard.number.split(" ").last}",
                    style:
                        const TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                Image.asset(ImageUtils.logoMasterCard, width: 30)
              ],
            ),
          )
        ],
      ),
    );
  }
}
