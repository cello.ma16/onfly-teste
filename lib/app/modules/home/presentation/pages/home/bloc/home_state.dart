import 'package:equatable/equatable.dart';
import 'package:onfly_teste/app/core/domain/entities/travel.dart';
import 'package:onfly_teste/app/modules/home/domain/entities/credit_card.dart';

import '../../../../../../core/domain/entities/expense.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitialState extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoadingState extends HomeState {

  @override
  List<Object> get props => [];
}

class HomeLoadedState extends HomeState {
  final Expense? expense;
  final Travel? travel;
  final CreditCard? creditCard;

  const HomeLoadedState({this.expense, this.travel, this.creditCard});

  HomeLoadedState copyWith({Expense? expense, Travel? travel, CreditCard? creditCard}) {
    return HomeLoadedState(
      expense: expense ?? this.expense,
      travel: travel ?? this.travel,
      creditCard: creditCard ?? this.creditCard,
    );
  }

  @override
  List<Object> get props => [expense!, travel!, creditCard!];
}
