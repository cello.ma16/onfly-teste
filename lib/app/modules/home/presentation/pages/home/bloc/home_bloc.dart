import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onfly_teste/app/modules/home/domain/entities/credit_card.dart';
import '../../../../../../core/domain/entities/expense.dart';
import '../../../../../../core/domain/entities/travel.dart';
import '../../../../domain/usecases/get_credit_card_usecase.dart';
import '../../../../domain/usecases/get_last_expense_usecase.dart';
import '../../../../domain/usecases/get_next_gravel_usecase.dart';
import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final IGetCreditCardUsecase getCreditCard;
  final IGetLastExpenseUsecase getLastExpenses;
  final IGetNextTravelUsecase getNextTravel;

  HomeBloc({
    required this.getCreditCard,
    required this.getLastExpenses,
    required this.getNextTravel,
  }) : super(HomeInitialState()){
    on<FetchAllHomeEvent>(_onFetchAllHome);
  }

  Future<void> _onFetchAllHome(FetchAllHomeEvent event, Emitter<HomeState> emit) async {
    emit(HomeLoadingState());

    final creditCard = await _onCreditCard();
    final expense = await _onExpense();
    final travel = await _onTravel();

    print(creditCard);

    emit(HomeLoadedState(
      creditCard: creditCard,
      expense: expense,
      travel: travel,
    ));
  }

  Future<CreditCard?> _onCreditCard() async {
    final result = await getCreditCard();
    return result.fold(
      (l) {
        return null;
      },
      (r) {
        return r;
      },
    );
  }

  Future<Expense?> _onExpense() async {
    final result = await getLastExpenses();
    return result.fold(
      (l) {
        return null;
      },
      (r) {
        return r;
      },
    );
  }

  Future<Travel?> _onTravel() async {
    final result = await getNextTravel();
    return result.fold(
      (l) {
        return null;
      },
      (r) {
        return r;
      },
    );
  }
}