import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/travels/exceptions/travels_domain_exceptions.dart';

import '../../../core/domain/entities/travel.dart';

typedef GetTravelsType = Either<TravelsDomainException, List<Travel>>;