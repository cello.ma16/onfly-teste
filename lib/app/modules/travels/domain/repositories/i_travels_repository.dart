import 'package:dartz/dartz.dart';

import '../../exceptions/travels_domain_exceptions.dart';
import '../../types/travels_types.dart';
import '../../../../core/domain/entities/travel.dart';

abstract class ITravelsRepository{
  Future<GetTravelsType> getTravels();
}