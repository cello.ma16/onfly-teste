import 'package:onfly_teste/app/modules/travels/domain/repositories/i_travels_repository.dart';

import '../../types/travels_types.dart';

abstract class IGetTravelsUsecase {
  Future<GetTravelsType> call();
}

class GetTravelsUsecase implements IGetTravelsUsecase {

  final ITravelsRepository travelsRepository;

  GetTravelsUsecase({required this.travelsRepository});

  @override
  Future<GetTravelsType> call() async {
    return await travelsRepository.getTravels();
  }
}