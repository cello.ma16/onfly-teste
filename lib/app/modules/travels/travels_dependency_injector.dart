import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/dependencies/i_dependency_injector.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/i_travels_dao.dart';
import 'package:onfly_teste/app/modules/travels/domain/repositories/i_travels_repository.dart';
import 'package:onfly_teste/app/modules/travels/domain/usecases/get_travels_usecase.dart';
import 'package:onfly_teste/app/modules/travels/external/datasources/travels_datasource.dart';
import 'package:onfly_teste/app/modules/travels/infra/datasources/i_travels_datasource.dart';
import 'package:onfly_teste/app/modules/travels/infra/repositories/travels_repository.dart';
import 'package:onfly_teste/app/modules/travels/presentation/pages/travels/blocs/travels/travels_bloc.dart';

class TravelsDependencyInjector implements IDependencyInjector {

  final _getIt = GetIt.instance;


  @override
  void init() {
    //Datasources
    _getIt.registerLazySingleton<ITravelsDatasource>(() => TravelsDatasource(travelsDao: _getIt.get<ITravelsDao>()));
    //Repositories
    _getIt.registerLazySingleton<ITravelsRepository>(() => TravelsRepository(travelsDatasource: _getIt.get<ITravelsDatasource>()));
    //Usecases
    _getIt.registerLazySingleton<IGetTravelsUsecase>(() => GetTravelsUsecase(travelsRepository: _getIt.get<ITravelsRepository>()));
    //Blocs
    _getIt.registerFactory<TravelsBloc>(() => TravelsBloc(getTravelsUsecase: _getIt.get<IGetTravelsUsecase>()));
  }
}