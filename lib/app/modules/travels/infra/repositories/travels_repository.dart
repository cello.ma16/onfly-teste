import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:onfly_teste/app/modules/travels/infra/datasources/i_travels_datasource.dart';

import '../../domain/repositories/i_travels_repository.dart';
import '../../exceptions/travels_domain_exceptions.dart';
import '../../types/travels_types.dart';

class TravelsRepository implements ITravelsRepository {

  final ITravelsDatasource travelsDatasource;

  TravelsRepository({required this.travelsDatasource});

  @override
  Future<GetTravelsType> getTravels() async {
    try {
      final travels = await travelsDatasource.getTravels();
      return Right(travels);
    } catch (e) {
      return Left(TravelsUnexpectedException());
    }
  }
}