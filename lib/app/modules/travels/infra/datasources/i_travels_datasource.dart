import '../../../../core/domain/entities/travel.dart';

abstract class ITravelsDatasource {
  Future<List<Travel>> getTravels();
}