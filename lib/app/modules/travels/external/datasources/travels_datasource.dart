import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/i_travels_dao.dart';
import 'package:onfly_teste/app/core/domain/entities/travel.dart';
import 'package:onfly_teste/app/modules/travels/infra/datasources/i_travels_datasource.dart';

class TravelsDatasource implements ITravelsDatasource{

  final ITravelsDao travelsDao;

  TravelsDatasource({required this.travelsDao});

  @override
  Future<List<Travel>> getTravels() async {
    final travels = await travelsDao.getTravels();
    return travels.map((e) => Travel.fromJson(e)).toList();
  }
}