import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../core/utils/color_utils.dart';
import '../../../../core/utils/image_utils.dart';
import '../../../../core/domain/entities/travel.dart';

class BoardPassDialog extends StatelessWidget {
  final Travel travel;
  const BoardPassDialog({super.key, required this.travel});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(20),
      child: Container(
        height: 450,
        decoration: BoxDecoration(
          color: ColorUtils.whiteColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 50,
              decoration: BoxDecoration(
                color: ColorUtils.primaryColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 50,
                    width: 50,
                  ),
                  Image.asset(ImageUtils.logoWhite, width: 100),
                  SizedBox(
                    height: 50,
                    width: 50,
                    child: Align(
                      alignment: Alignment.center,
                      child: IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(Icons.close,
                            color: ColorUtils.whiteColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            const Text('Cartão de Embarque',
                style:  TextStyle(
                    fontSize: 16, fontWeight: FontWeight.bold)),
            const SizedBox(height: 20),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Nome',
                                style: TextStyle(fontSize: 10)),
                            Text(travel.boardingPass.passengerName,
                                style: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                        const SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Data',
                                style: TextStyle(fontSize: 10)),
                            Text(DateFormat('dd/MM/yyyy').format(travel.date),
                                style: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(travel.cityFrom,
                                  style: const TextStyle(fontSize: 10)),
                              Text(
                                "${travel.boardingPass.airportFrom} - ${travel.boardingPass.airportFromCode}",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: const TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                  DateFormat('H:mm').format(
                                      travel.boardingPass.departureTime),
                                  style: const TextStyle(fontSize: 10)),
                            ],
                          ),
                        ),
                        const SizedBox(width: 5),
                        Column(
                          children: [
                            const RotatedBox(
                              quarterTurns: 1,
                              child: Icon(Icons.flight, size: 20),
                            ),
                            Text(travel.boardingPass.flightNumber,
                                style: const TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(travel.cityTo,
                                  style: const TextStyle(fontSize: 10)),
                              Text(
                                  "${travel.boardingPass.airportTo} - ${travel.boardingPass.airportToCode}",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold)),
                              Text(
                                  DateFormat('H:mm')
                                      .format(travel.boardingPass.arrivalTime),
                                  style: const TextStyle(fontSize: 10)),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Companhia',
                                    style: TextStyle(fontSize: 10)),
                                Text(travel.boardingPass.flightCompany,
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            const SizedBox(height: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Portão',
                                    style: TextStyle(fontSize: 10)),
                                Text(travel.boardingPass.gate,
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Assento',
                                    style: TextStyle(fontSize: 10)),
                                Text(travel.boardingPass.seat,
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            const SizedBox(height: 10),
                            const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Seção',
                                    style: TextStyle(fontSize: 10)),
                                Text("2",
                                    style:  TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                              ],
                            )
                          ],
                        ),
                        const Spacer(),
                        QrImageView(
                          data: "www.google.com",
                          size: 90,
                          padding: const EdgeInsets.all(10),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: ColorUtils.primaryColor, width: 0.5),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              'Início do Embarque às ${DateFormat("H:mm").format(travel.boardingPass.boardingStart)}',
                              style: const TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold)),
                          Text(
                              'Fim do Embarque às ${DateFormat("H:mm").format(travel.boardingPass.boardingEnd)}',
                              style: const TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
