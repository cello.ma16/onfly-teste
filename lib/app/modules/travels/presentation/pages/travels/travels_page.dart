import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../../../../core/presentation/widgets/base_page_body.dart';
import '../../../../../core/presentation/widgets/travel_card.dart';
import 'blocs/travels/travels_bloc.dart';
import 'blocs/travels/travels_events.dart';
import 'blocs/travels/travels_states.dart';

class TravelsPage extends StatefulWidget {
  const TravelsPage({super.key});

  @override
  State<TravelsPage> createState() => _TravelsPageState();
}

class _TravelsPageState extends State<TravelsPage> {
  final _bloc = GetIt.instance.get<TravelsBloc>();

  @override
  void initState() {
    super.initState();
    _bloc.add(TravelsFetchEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BasePageBody(
      title: 'Viagens',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 20),
            const Text('Suas próximas viagens',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            const SizedBox(height: 20),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () async {
                  _bloc.add(TravelsFetchEvent());
                },
                child: BlocBuilder(
                    bloc: _bloc,
                    builder: (context, state) {
                  if (state is TravelsLoading) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state is TravelsLoaded) {
                    if (state.travels.isEmpty) {
                      return const Center(child: Text('Nenhuma viagem encontrada'));
                    } else {
                      return ListView.builder(
                        padding: const EdgeInsets.all(0),
                        itemCount: state.travels.length,
                        itemBuilder: (context, index) {
                          return TravelCard(
                            travel: state.travels[index],
                          );
                        },
                      );
                    }
                  } else {
                    return const SizedBox();
                  }
                }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
