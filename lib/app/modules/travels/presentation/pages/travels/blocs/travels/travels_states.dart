import 'package:equatable/equatable.dart';

import '../../../../../../../core/domain/entities/travel.dart';


abstract class TravelsState extends Equatable {
  const TravelsState();
}

class TravelsInitial extends TravelsState {
  const TravelsInitial();

  @override
  List<Object> get props => [];
}

class TravelsLoading extends TravelsState {
  const TravelsLoading();

  @override
  List<Object> get props => [];
}

class TravelsLoaded extends TravelsState {
  final List<Travel> travels;

  const TravelsLoaded({required this.travels,});

  @override
  List<Object> get props => [
    travels,
  ];
}
