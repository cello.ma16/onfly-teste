import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onfly_teste/app/modules/travels/presentation/pages/travels/blocs/travels/travels_events.dart';
import 'package:onfly_teste/app/modules/travels/presentation/pages/travels/blocs/travels/travels_states.dart';

import '../../../../../domain/usecases/get_travels_usecase.dart';


class TravelsBloc extends Bloc<TravelsEvent, TravelsState> {

  final IGetTravelsUsecase getTravelsUsecase;

  TravelsBloc({required this.getTravelsUsecase}) : super(const TravelsInitial()) {
    on<TravelsFetchEvent>(_travelsFetchEvent);
  }

  Future<void> _travelsFetchEvent(
      TravelsFetchEvent event, Emitter<TravelsState> emitter) async {
    emitter(const TravelsLoading());

    final result = await getTravelsUsecase();

    result.fold(
      (failure) {
        log(failure.message);
        emitter(const TravelsLoaded(travels: []));
      },
      (travels) {
        emitter(TravelsLoaded(travels: travels));
      },
    );
  }
}
