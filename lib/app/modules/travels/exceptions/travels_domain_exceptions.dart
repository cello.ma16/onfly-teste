abstract class TravelsDomainException implements Exception {
  final String message;

  TravelsDomainException(this.message);
}

class TravelsUnexpectedException extends TravelsDomainException {
  TravelsUnexpectedException() : super("Um erro inesperado ocorreu, tente novamente mais tarde");
}