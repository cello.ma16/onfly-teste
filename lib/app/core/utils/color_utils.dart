import 'package:flutter/material.dart';

class ColorUtils {
  static Color get primaryColor => const Color(0xff019EFB);

  static Color get borderColor => const Color(0xff6B6B6B);

  static Color get successColor => const Color(0xff00D715);

  static Color get errorColor => const Color(0xffFF3636);

  static Color get fontBlackColor => const Color(0xff000000);

  static Color get whiteColor => const Color(0xffFFFFFF);
}
