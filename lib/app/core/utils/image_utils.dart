class ImageUtils{
  static String get logoColored => 'assets/images/logo_colored.png';

  static String get logoWhite => 'assets/images/logo_white.png';

  static String get logoMasterCard => 'assets/images/logo_mastercard.png';

  static String get iconDashboard => 'assets/images/icon_dashboard.png';

  static String get iconHome => 'assets/images/icon_home.png';

  static String get iconTravels => 'assets/images/icon_travels.png';

  static String get iconExpenses => 'assets/images/icon_expenses.png';
}