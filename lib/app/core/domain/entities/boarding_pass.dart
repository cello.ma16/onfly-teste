class BoardingPass{
  final int idTravel;
  final String passengerName;
  final String flightNumber;
  final String seat;
  final String gate;
  final DateTime boardingStart;
  final DateTime boardingEnd;
  final DateTime departureTime;
  final DateTime arrivalTime;
  final String flightCompany;
  final String airportFrom;
  final String airportTo;
  final String airportFromCode;
  final String airportToCode;
  final String session;

  BoardingPass({
    required this.idTravel,
    required this.passengerName,
    required this.flightNumber,
    required this.seat,
    required this.gate,
    required this.boardingStart,
    required this.boardingEnd,
    required this.departureTime,
    required this.arrivalTime,
    required this.flightCompany,
    required this.airportFrom,
    required this.airportTo,
    required this.airportFromCode,
    required this.airportToCode,
    required this.session,
  });

  factory BoardingPass.fromJson(Map<String, dynamic> json) {
    return BoardingPass(
      idTravel: json['idTravel'],
      passengerName: json['passengerName'],
      flightNumber: json['flightNumber'],
      seat: json['seat'],
      gate: json['gate'],
      session: json['session'],
      boardingStart: DateTime.parse(json['boardingTime']),
      boardingEnd: DateTime.parse(json['boardingTime']),
      departureTime: DateTime.parse(json['departureTime']),
      arrivalTime: DateTime.parse(json['arrivalTime']),
      flightCompany: json['flightCompany'],
      airportFrom: json['airportFrom'],
      airportTo: json['airportTo'],
      airportFromCode: json['airportFromCode'],
      airportToCode: json['airportToCode'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'idTravel': idTravel,
      'passengerName': passengerName,
      'flightNumber': flightNumber,
      'seat': seat,
      'gate': gate,
      'boardingStart': boardingStart,
      'boardingEnd': boardingEnd,
      'departureTime': departureTime,
      'arrivalTime': arrivalTime,
      'flightCompany': flightCompany,
      'airportFrom': airportFrom,
      'airportTo': airportTo,
      'airportFromCode': airportFromCode,
      'airportToCode': airportToCode,
      'session': session,
    };
  }

  @override
  String toString() {
    return 'BoardingPass(passengerName: $passengerName, flightNumber: $flightNumber, seat: $seat, gate: $gate, boardingStart: $boardingStart, boardingEnd: $boardingEnd, departureTime: $departureTime, arrivalTime: $arrivalTime, flightCompany: $flightCompany, airportFrom: $airportFrom, airportTo: $airportTo, airportFromCode: $airportFromCode, airportToCode: $airportToCode, session: $session)';
  }
}