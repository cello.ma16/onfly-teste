import 'package:equatable/equatable.dart';

enum ExpenseCategoryEnum { FOOD, HOTEL, FUEL, CAR_RENT, OTHER }

class Expense extends Equatable {
  final int? id;
  final ExpenseCategoryEnum category;
  final double value;
  final String description;
  final DateTime date;

  const Expense(
      {this.id,
      required this.category,
      required this.value,
      required this.date,
      required this.description});

  @override
  String toString() {
    return 'Expense{id: $id, category: ${category.name}, value: $value, date: $date, description: $description}';
  }

  factory Expense.fromMap(Map<String, dynamic> map) {
    return Expense(
        id: map['id'],
        category: ExpenseCategoryEnum.values
            .where((element) => element.name == map['category'])
            .firstOrNull ?? ExpenseCategoryEnum.OTHER,
        value: map['value'],
        description: map['description'],
        date: DateTime.parse(map['date']));
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'category': category.index,
      'value': value,
      'date': date.toIso8601String(),
      'description': description
    };
  }

  @override
  List<Object?> get props => [id, category, value, date, description];
}
