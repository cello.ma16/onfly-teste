class User{
  final int id;
  final String name;
  final String email;
  final String? password;

  User({required this.id, required this.name, required this.email, this.password});

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, password: $password}';
  }

  User copyWith({
    int? id,
    String? name,
    String? email,
    String? password,
  }) {
    return User(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  //Adapter for handling data from database
  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'],
      name: map['name'],
      email: map['email'],
      password: map['password'],
    );
  }
}