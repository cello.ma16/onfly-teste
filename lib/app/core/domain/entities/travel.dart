import 'boarding_pass.dart';

class Travel{
  final int id;
  final String cityFrom;
  final String cityTo;
  final DateTime date;
  final BoardingPass boardingPass;

  Travel({
    required this.id,
    required this.cityFrom,
    required this.cityTo,
    required this.date,
    required this.boardingPass,
  });

  factory Travel.fromJson(Map<String, dynamic> json) {
    return Travel(
      id: json['id'],
      cityFrom: json['cityFrom'],
      cityTo: json['cityTo'],
      date: DateTime.parse(json['date']),
      boardingPass: BoardingPass.fromJson(json['boardingPass']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'cityFrom': cityFrom,
      'cityTo': cityTo,
      'date': date,
      'boardingPass': boardingPass.toJson(),
    };
  }
}