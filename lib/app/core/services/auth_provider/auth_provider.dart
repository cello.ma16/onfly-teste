import 'dart:async';

import '../../domain/entities/user.dart';

abstract class IAuthProvider {
  Stream<User?> get user;
  void addUser(User? user);
  void dispose();
  void removeUser();
}

class AuthProvider implements IAuthProvider {
  final _userStreamController = StreamController<User?>.broadcast();

  @override
  Stream<User?> get user => _userStreamController.stream;

  @override
  void addUser(User? user) {
    _userStreamController.add(user);
  }

  @override
  void dispose() {
    _userStreamController.close();
  }

  @override
  void removeUser() {
    _userStreamController.add(null);
  }
}
