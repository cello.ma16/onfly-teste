import 'dart:developer';

import 'package:sqflite/sqflite.dart';

class SqliteDatabase {
  static const String _databaseName = "database.db";
  static const int _databaseVersion = 2;
  static late final Database? _database;
  static SqliteDatabase? _instance;

  SqliteDatabase._();

  static SqliteDatabase? get instance {
    _instance ??= SqliteDatabase._();
    return _instance;
  }

  static Database? get database => _database;

  Future<void> init() async {
    await _initDatabase();
  }

  static Future<void> _initDatabase() async {
    try {
      final String databasePath = await getDatabasesPath();
      String path = '$databasePath/$_databaseName';
      _database = await openDatabase(
        path,
        version: _databaseVersion,
        onCreate: (db, version) async {
          await db.execute(
              'CREATE TABLE Users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL);');
          await db.execute(
              'CREATE TABLE Travels (id INTEGER PRIMARY KEY AUTOINCREMENT, cityFrom TEXT NOT NULL, cityTo TEXT NOT NULL, date TEXT NOT NULL);');
          await db.execute(
              'CREATE TABLE BoardingPasses (id INTEGER PRIMARY KEY AUTOINCREMENT, passengerName TEXT NOT NULL, flightNumber TEXT NOT NULL, session TEXT NOT NULL, seat TEXT NOT NULL, gate TEXT NOT NULL, boardingTime TEXT NOT NULL, departureTime TEXT NOT NULL, arrivalTime TEXT NOT NULL, flightCompany TEXT NOT NULL, airportFrom TEXT NOT NULL, airportTo TEXT NOT NULL, airportFromCode TEXT NOT NULL, airportToCode TEXT NOT NULL, idTravel INTEGER NOT NULL, FOREIGN KEY (idTravel) REFERENCES Travels (id) ON DELETE CASCADE ON UPDATE CASCADE);');
          await db.execute(
              'CREATE TABLE Expenses (id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT NOT NULL, value REAL NOT NULL, date TEXT NOT NULL, category TEXT NOT NULL);'
          );
          await db.execute(
              'CREATE TABLE CreditCards (id INTEGER PRIMARY KEY AUTOINCREMENT, number TEXT NOT NULL, name TEXT NOT NULL, \"limit\" REAL NOT NULL, flag TEXT NOT NULL);'
          );

          //Não será feita a criptografia da senha, pois é apenas um exemplo
          await db.insert("Users", {
            "id": 1,
            "name": "Admin",
            "email": "admin@onfly-teste.com",
            "password": "admin"
          });

          await db.insert("Travels", {
            "id": 1,
            "cityFrom": "São Paulo",
            "cityTo": "Rio de Janeiro",
            "date": "2024-12-12T00:00:00"
          });

          await db.insert("BoardingPasses", {
            "passengerName": "Admin",
            "flightNumber": "123",
            "session": "1",
            "seat": "1A",
            "gate": "1",
            "boardingTime": "2022-12-12T12:00:00",
            "departureTime": "2022-12-12T13:00:00",
            "arrivalTime": "2022-12-12T14:00:00",
            "flightCompany": "GOL",
            "airportFrom": "Guarulhos",
            "airportTo": "Santos Dumont",
            "airportFromCode": "GRU",
            "airportToCode": "SDU",
            "idTravel": 1
          });

          await db.insert("Expenses", {
            "id": 1,
            "description": "Almoço no aeroporto",
            "value": 50.0,
            "date": "2022-12-12T00:00:00",
            "category": "FOOD"
          });

          await db.insert("Expenses", {
            "id": 2,
            "value": 30.0,
            "description": "Uber para o aeroporto",
            "date": "2022-12-12T00:00:00",
            "category": "OTHER"
          });

          await db.insert("CreditCards", {
            "id": 1,
            "number": "1234 5678 1234 5678",
            "name": "Admin",
            "limit": 1000.0,
            "flag": "MASTERCARD"
          });
        },
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
