abstract class ITravelsDao {
  Future<List<Map<String, dynamic>>> getTravels();
  Future<Map<String, dynamic>?> getNextTravel();
}