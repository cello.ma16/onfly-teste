import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/i_travels_dao.dart';
import 'package:sqflite/sqflite.dart';

class SqliteTravelsDao implements ITravelsDao {

  final Database database;

  SqliteTravelsDao({required this.database});

  @override
  Future<List<Map<String, dynamic>>> getTravels() async {
    //Just to return as a map containing the travel and its boarding passes structured
    //Maybe in the future it can be improved
    final resultTravels = await database.rawQuery(""
        "SELECT * FROM Travels "
        "ORDER BY Travels.date ASC;"
        "");

    final resultBoardingPasses = await database.rawQuery(""
        "SELECT * FROM BoardingPasses "
        "ORDER BY BoardingPasses.boardingTime ASC;"
        "");

    final List<Map<String, dynamic>> result = [];

    for (final travel in resultTravels) {
      final boardingPasses = resultBoardingPasses.where((element) => element["idTravel"] == travel["id"]).toList().first;
      result.add({
        ...travel,
        "boardingPass": boardingPasses
      });
    }

    return result;
  }

  @override
  Future<Map<String, dynamic>?> getNextTravel() async {
    final DateTime now = DateTime.now();

    print(now.toIso8601String());

    final result = await database.rawQuery(""
        "SELECT * FROM Travels "
        "WHERE strftime('%s', Travels.date) >= strftime('%s', ?) "
        "ORDER BY strftime('%s', Travels.date) ASC "
        "LIMIT 1;"
        "", [now.toIso8601String()]);

    if(result.isEmpty) return null;

    final resultBoardingPasses = await database.rawQuery(""
        "SELECT * FROM BoardingPasses "
        "WHERE BoardingPasses.idTravel = ? "
        "", [result.first["id"]]);

    return {
      ...result.first,
      "boardingPass": resultBoardingPasses.first
    };
  }
}