abstract class ICreditCardDao{
  Future<Map<String, dynamic>?> getCreditCard();
}