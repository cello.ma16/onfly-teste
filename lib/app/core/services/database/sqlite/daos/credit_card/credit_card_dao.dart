import 'package:sqflite/sqflite.dart';

import '../../../../../../modules/home/domain/entities/credit_card.dart';
import 'i_credit_card_dao.dart';

class SqliteCreditCardDao implements ICreditCardDao {
  final Database database;

  SqliteCreditCardDao({required this.database});

  @override
  Future<Map<String, dynamic>?> getCreditCard() async {
    final result = await database.query('CreditCards');
    if (result.isEmpty) return null;
    return result.first;
  }
}