import '../../../../../../modules/authentication/params/signin_params.dart';

abstract class IAuthenticationDao {
  Future<Map<String, dynamic>?> signin(ISigninParams params);
}