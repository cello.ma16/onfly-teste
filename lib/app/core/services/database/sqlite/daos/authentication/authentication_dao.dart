import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';
import 'package:sqflite/sqflite.dart';

import 'i_authentication_dao.dart';

class SqliteAuthenticationDao implements IAuthenticationDao {
  final Database database;

  SqliteAuthenticationDao({required this.database});

  @override
  Future<Map<String, dynamic>?> signin(ISigninParams params) async {

    final result = await database.query("USERS",
        where: "email = ? AND password = ?",
        whereArgs: [params.email, params.password]);

    if (result.isEmpty) return null;

    return result.first;
  }
}
