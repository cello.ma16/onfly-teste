abstract class IExpenseDao {
  Future<List<Map<String, dynamic>>> getExpenses();

  Future<void> saveExpense({required Map<String, dynamic> params});

  Future<void> updateExpense({required Map<String, dynamic> params});

  Future<List<Map<String, dynamic>>> getExpensesBetweenDates(
      {required DateTime startDate, required DateTime endDate});

  Future<Map<String, dynamic>?> getLastExpense();
}
