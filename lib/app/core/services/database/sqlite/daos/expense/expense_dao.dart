import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:sqflite/sqflite.dart';

class SqliteExpenseDao implements IExpenseDao {
  final Database database;

  SqliteExpenseDao({required this.database});

  @override
  Future<List<Map<String, dynamic>>> getExpenses() async {
    final result = await database.query("Expenses", orderBy: "date DESC");
    return result;
  }

  @override
  Future<void> saveExpense({required Map<String, dynamic> params}) async {
    await database.insert("Expenses", params);
  }

  @override
  Future<void> updateExpense({required Map<String, dynamic> params}) async {
    await database
        .update("Expenses", params, where: "id = ?", whereArgs: [params["id"]]);
  }

  @override
  Future<List<Map<String, dynamic>>> getExpensesBetweenDates(
      {required DateTime startDate, required DateTime endDate}) async {
    final result = await database.query("Expenses",
        where: "date BETWEEN ? AND ?",
        whereArgs: [startDate.toIso8601String(), endDate.toIso8601String()]);
    return result;
  }

  @override
  Future<Map<String, dynamic>?> getLastExpense() async {
    final result = await database.query("Expenses", orderBy: "date DESC", limit: 1);
    if (result.isEmpty) return null;
    return result.first;
  }
}
