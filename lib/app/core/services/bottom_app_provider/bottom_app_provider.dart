abstract class IBottomAppProvider {
  int get currentPage;
  void changePage(int index);
}

class BottomAppProvider implements IBottomAppProvider {
  int _pageIndex = 0;

  @override
  Future<void> changePage(int index) async {
    _pageIndex = index;
  }

  @override
  int get currentPage => _pageIndex;
}