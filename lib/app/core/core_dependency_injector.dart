import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/presentation/widgets/bottom_bar/bloc/bottom_app_bloc.dart';
import 'package:onfly_teste/app/core/services/auth_provider/auth_provider.dart';
import 'package:onfly_teste/app/core/services/bottom_app_provider/bottom_app_provider.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/authentication/authentication_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/authentication/i_authentication_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/credit_card/credit_card_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/credit_card/i_credit_card_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/expense_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/i_travels_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/travels_dao.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/sqlite_database.dart';
import 'dependencies/i_dependency_injector.dart';

class CoreDependencyInjector implements IDependencyInjector {
  final _getIt = GetIt.instance;

  @override
  Future<void> init() async {
    //SERVICES
    //DAO
    _getIt.registerLazySingleton<IAuthenticationDao>(
      () => SqliteAuthenticationDao(database: SqliteDatabase.database!),
    );
    _getIt.registerLazySingleton<ITravelsDao>(
      () => SqliteTravelsDao(database: SqliteDatabase.database!),
    );
    _getIt.registerLazySingleton<IExpenseDao>(
        () => SqliteExpenseDao(database: SqliteDatabase.database!));
    _getIt.registerLazySingleton<ICreditCardDao>(() => SqliteCreditCardDao(database: SqliteDatabase.database!));

    //PROVIDERS
    _getIt.registerLazySingleton<IAuthProvider>(() => AuthProvider());
    _getIt.registerLazySingleton<IBottomAppProvider>(() => BottomAppProvider());

    //BLOCS
    _getIt.registerLazySingleton<BottomAppBloc>(() => BottomAppBloc());
  }
}
