import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:onfly_teste/app/core/utils/color_utils.dart';
import 'package:onfly_teste/app/modules/dashboard/presentation/pages/dashboard/dashboard_page.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_form/expense_form_page.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_list/expenses_list_page.dart';
import 'package:onfly_teste/app/modules/home/presentation/pages/home/home_page.dart';
import 'package:onfly_teste/app/modules/travels/presentation/pages/travels/travels_page.dart';

import 'bloc/bottom_app_bloc.dart';
import 'bloc/bottom_app_event.dart';
import 'bloc/bottom_app_state.dart';

class BottomBar extends StatelessWidget {
  final _bloc = GetIt.instance.get<BottomAppBloc>();

  BottomBar({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (context, state) {
          final index = (state as BottomAppInitial).currentPage;

          return Container(
            height: 60,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: const Offset(0, 1),
                ),
              ],
            ),
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _bloc.add(ChangeBottomAppEvent(index: 0));
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => const HomePage()));
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: index == 0
                              ? ColorUtils.primaryColor
                              : ColorUtils.borderColor,
                        ),
                        Text(
                          "Home",
                          style: TextStyle(
                              fontSize: 12,
                              color: index == 0
                                  ? ColorUtils.primaryColor
                                  : ColorUtils.borderColor),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _bloc.add(ChangeBottomAppEvent(index: 1));
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const TravelsPage()));
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.wallet_travel,
                            color: index == 1
                                ? ColorUtils.primaryColor
                                : ColorUtils.borderColor),
                        Text("Viagens",
                            style: TextStyle(
                                fontSize: 12,
                                color: index == 1
                                    ? ColorUtils.primaryColor
                                    : ColorUtils.borderColor)),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Positioned(
                        top: -30,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const ExpenseForm()));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorUtils.primaryColor),
                            width: 60,
                            height: 60,
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _bloc.add(ChangeBottomAppEvent(index: 2));
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ExpensesListPage()));
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.money,
                            color: index == 2
                                ? ColorUtils.primaryColor
                                : ColorUtils.borderColor),
                        Text("Despesas",
                            style: TextStyle(
                                fontSize: 12,
                                color: index == 2
                                    ? ColorUtils.primaryColor
                                    : ColorUtils.borderColor)),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _bloc.add(ChangeBottomAppEvent(index: 3));
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const DashboardPage()));
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.dashboard,
                            color: index == 3
                                ? ColorUtils.primaryColor
                                : ColorUtils.borderColor),
                        Text("Info",
                            style: TextStyle(
                                fontSize: 12,
                                color: index == 3
                                    ? ColorUtils.primaryColor
                                    : ColorUtils.borderColor)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
