abstract class BottomAppEvent {}

class ChangeBottomAppEvent extends BottomAppEvent {
  final int index;

  ChangeBottomAppEvent({required this.index});
}