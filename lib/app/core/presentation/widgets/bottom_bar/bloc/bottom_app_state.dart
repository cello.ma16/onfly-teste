import 'package:equatable/equatable.dart';

abstract class BottomAppState extends Equatable {
  const BottomAppState();
}

class BottomAppInitial extends BottomAppState {
  final int currentPage;

  const BottomAppInitial({required this.currentPage});

  BottomAppInitial copyWith({int? currentPage}) {
    return BottomAppInitial(
      currentPage: currentPage ?? this.currentPage,
    );
  }

  @override
  List<Object> get props => [
    currentPage
  ];
}