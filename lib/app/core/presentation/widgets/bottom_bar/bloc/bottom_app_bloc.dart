import 'package:flutter_bloc/flutter_bloc.dart';

import 'bottom_app_event.dart';
import 'bottom_app_state.dart';

class BottomAppBloc extends Bloc<BottomAppEvent, BottomAppState> {
  BottomAppBloc() : super(const BottomAppInitial(currentPage: 0)){
    on<ChangeBottomAppEvent>(_onChangeBottomApp);
  }

  void _onChangeBottomApp(ChangeBottomAppEvent event, Emitter<BottomAppState> emitter) {
    final state = this.state as BottomAppInitial;
    emitter(state.copyWith(currentPage: event.index));
  }
}