import 'package:flutter/material.dart';
import 'package:onfly_teste/app/core/presentation/widgets/bottom_bar/bottom_bar.dart';
import 'package:onfly_teste/app/core/utils/color_utils.dart';

class BasePageBody extends StatelessWidget {
  final String title;
  final Widget? body;
  final bool shouldShowBottomAppBar;
  final bool shouldShowBackButton;

  const BasePageBody(
      {super.key,
      required this.title,
      this.body,
      this.shouldShowBottomAppBar = true,
      this.shouldShowBackButton = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: shouldShowBottomAppBar ? BottomBar() : null,
      resizeToAvoidBottomInset: true,
      backgroundColor: ColorUtils.whiteColor,
      appBar: _buildAppBar(context),
      body: body,
    );
  }

  PreferredSize _buildAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(MediaQuery.of(context).size.width, 55),
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40),
            bottomRight: Radius.circular(40),
          ),
        ),
        child: Row(
          children: [
            Expanded(
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: shouldShowBackButton
                        ? IconButton(
                            icon: Icon(Icons.arrow_back,
                                color: ColorUtils.whiteColor),
                            onPressed: () => Navigator.of(context).pop())
                        : Container())),
            Center(
              child: Text(
                title,
                style: TextStyle(
                    color: ColorUtils.whiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            const Spacer()
          ],
        ),
      ),
    );
  }
}
