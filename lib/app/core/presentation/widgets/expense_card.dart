import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/presentation/pages/expenses_form/expense_form_page.dart';

import '../../utils/color_utils.dart';

class ExpenseCard extends StatelessWidget {
  final Expense expense;

  const ExpenseCard({super.key, required this.expense});

  String getNameFromCategory(ExpenseCategoryEnum categoryEnum) {
    switch (categoryEnum) {
      case ExpenseCategoryEnum.FOOD:
        return 'Alimentação';
      case ExpenseCategoryEnum.FUEL:
        return 'Combutível';
      case ExpenseCategoryEnum.HOTEL:
        return 'Hospedagem';
      case ExpenseCategoryEnum.CAR_RENT:
        return 'Aluguel de veículo';
      case ExpenseCategoryEnum.OTHER:
        return 'Outros';
    }
  }

  IconData iconFromCategory(ExpenseCategoryEnum categoryEnum) {
    switch (categoryEnum) {
      case ExpenseCategoryEnum.FOOD:
        return Icons.restaurant;
      case ExpenseCategoryEnum.FUEL:
        return Icons.local_gas_station;
      case ExpenseCategoryEnum.HOTEL:
        return Icons.hotel;
      case ExpenseCategoryEnum.CAR_RENT:
        return Icons.directions_car;
      case ExpenseCategoryEnum.OTHER:
        return Icons.attach_money;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ExpenseForm(expense: expense),
        ));
      },
      child: Card(
        margin: const EdgeInsets.only(bottom: 20),
        color: ColorUtils.whiteColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: ColorUtils.borderColor, width: 0.5),
        ),
        child: Container(
          height: 105,
          padding: const EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(iconFromCategory(expense.category)),
                        const SizedBox(width: 5),
                        Text(getNameFromCategory(expense.category),
                            style: const TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Expanded(
                      child: Text(expense.description,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                              fontSize: 12)),
                    )
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(DateFormat('dd/MM/yyyy').format(expense.date),
                          style: const TextStyle(
                              fontSize: 10, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  Text("R\$ ${expense.value.toStringAsFixed(2)}",
                      style: const TextStyle(
                          fontSize: 14, fontWeight: FontWeight.bold))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
