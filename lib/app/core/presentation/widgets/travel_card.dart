import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:onfly_teste/app/core/utils/color_utils.dart';
import 'package:onfly_teste/app/core/domain/entities/travel.dart';
import 'package:onfly_teste/app/modules/travels/presentation/widgets/board_pass_dialog.dart';

class TravelCard extends StatelessWidget {
  final Travel travel;

  const TravelCard({super.key, required this.travel});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return BoardPassDialog(travel: travel);
          },
        );
      },
      child: Card(
        margin: const EdgeInsets.only(bottom: 20),
        color: ColorUtils.whiteColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: ColorUtils.borderColor, width: 0.5),
        ),
        child: Container(
          height: 105,
          padding: const EdgeInsets.all(12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('Origem', style: TextStyle(fontSize: 10)),
                          Text(travel.boardingPass.airportFromCode,
                              style: const TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold)),
                        ],
                      ),
                      const SizedBox(width: 5),
                      const Text(' - ',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                      const SizedBox(width: 5),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('Destino', style: TextStyle(fontSize: 10)),
                          Text(travel.boardingPass.airportToCode,
                              style: const TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(DateFormat('dd/MM/yyyy').format(travel.date),
                          style: const TextStyle(
                              fontSize: 10, fontWeight: FontWeight.bold)),
                      Text(
                          DateFormat('H:mm')
                              .format(travel.boardingPass.departureTime),
                          style: const TextStyle(
                              fontSize: 10, fontWeight: FontWeight.bold)),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Companhia", style: TextStyle(fontSize: 10)),
                      Text(travel.boardingPass.flightCompany,
                          style: const TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Text("Portão", style: TextStyle(fontSize: 10)),
                      Text(travel.boardingPass.gate,
                          style: const TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold)),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
