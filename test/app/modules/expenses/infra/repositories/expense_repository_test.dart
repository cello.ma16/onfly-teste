import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';
import 'package:onfly_teste/app/modules/expenses/infra/datasources/i_expense_datasource.dart';
import 'package:onfly_teste/app/modules/expenses/infra/repositories/expense_repository.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';

class MockExpenseDatasource extends Mock implements IExpenseDatasource {}

void main() {
  final IExpenseDatasource datasource = MockExpenseDatasource();
  final IExpensesRepository repository =
      ExpenseRepository(expenseDatasource: datasource);
  test("Should return a list of Expense", () async {
    // Arrange
    when(() => datasource.getExpenses()).thenAnswer((_) async => <Expense>[]);
    // Act
    final result = await repository.getExpenses();
    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<Expense>>());
  });

  test("Should return a ExpenseUnexpectedDomainException", () async {
    // Arrange
    when(() => datasource.getExpenses()).thenThrow(Exception());
    // Act
    final result = await repository.getExpenses();
    // Assert
    expect(result.isLeft(), true);
  });

  test("Should return a Right and true when saving", () async {
    // Arrange
    final params = SaveExpenseParams(
      date: DateTime.now(),
        description: "Test", value: 10, category: ExpenseCategoryEnum.FOOD);
    when(() => datasource.saveExpense(params: params)).thenAnswer((_) async {});
    // Act
    final result = await repository.saveExpense(params: params);
    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), true);
  });

  test("Should return a ExpenseUnexpectedDomainException when saving",
      () async {
    // Arrange
    final params = SaveExpenseParams(
      date: DateTime.now(),
        description: "Test", value: 10, category: ExpenseCategoryEnum.FOOD);
    when(() => datasource.saveExpense(params: params)).thenThrow(Exception());
    // Act
    final result = await repository.saveExpense(params: params);
    // Assert
    expect(result.isLeft(), true);
  });

  test("Should return a right and true when updating", () async {
    // Arrange
    final params = UpdateExpenseParams(
        newExpense: Expense(
            id: 1,
            description: "Test",
            date: DateTime.now(),
            value: 10,
            category: ExpenseCategoryEnum.FOOD),
        oldExpense: Expense(
            id: 1,
            description: "Test",
            date: DateTime.now(),
            value: 10,
            category: ExpenseCategoryEnum.FOOD));
    when(() => datasource.updateExpense(params: params))
        .thenAnswer((_) async {});

    // Act
    final result = await repository.updateExpense(params: params);

    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), true);
  });

  test("Should return Left and UnexpectedError when throw exception", () async {
    // Arrange
    final params = UpdateExpenseParams(
        newExpense: Expense(
            id: 1,
            description: "Test",
            date: DateTime.now(),
            value: 10,
            category: ExpenseCategoryEnum.FOOD),
        oldExpense: Expense(
            id: 1,
            description: "Test",
            date: DateTime.now(),
            value: 10,
            category: ExpenseCategoryEnum.FOOD));
    when(() => datasource.updateExpense(params: params)).thenThrow(Exception());

    // Act
    final result = await repository.updateExpense(params: params);

    // Assert
    expect(result.isLeft(), true);
  });
}
