import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/save_expense_usecase.dart';
import 'package:onfly_teste/app/modules/expenses/exceptions/expenses_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';

class MockExpenseRepository extends Mock implements IExpensesRepository {}

void main() {
  final IExpensesRepository repository = MockExpenseRepository();
  final ISaveExpenseUsecase usecase = SaveExpenseUsecase(
    expensesRepository: repository,
  );

  test("Should return true when save expense", () async {
    // Arrange
    final params = SaveExpenseParams(
      date: DateTime.now(),
        description: "Descrição teste",
        value: 10,
        category: ExpenseCategoryEnum.FOOD);
    when(() => repository.saveExpense(params: params))
        .thenAnswer((_) async => const Right(true));

    // Act
    final result = await usecase(params: params);

    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), true);
    verify(() => repository.saveExpense(params: params)).called(1);
  });

  test("Should return ExpensesInvalidParamsDomainException when params is invalid", () async {
    // Arrange
    final params = SaveExpenseParams(
      date: DateTime.now(),
        description: "",
        value: 0,
        category: ExpenseCategoryEnum.OTHER);

    // Act
    final result = await usecase(params: params);

    // Assert
    expect(result.fold(id, id), isA<ExpensesInvalidParamsDomainException>());
  });
}
