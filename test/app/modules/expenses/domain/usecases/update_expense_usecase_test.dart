import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/update_expense_usecase.dart';
import 'package:onfly_teste/app/modules/expenses/exceptions/expenses_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';

class MockExpenseRepository extends Mock implements IExpensesRepository {}

void main() {
  final IExpensesRepository repository = MockExpenseRepository();
  final IUpdateExpenseUsecase usecase =
      UpdateExpenseUsecase(expensesRepository: repository);

  test("Should return Right and true when updating", () async {
    // Arrange
    final date = DateTime.now();
    final params = UpdateExpenseParams(
        oldExpense: Expense(
            date: date,
            description: "Test",
            id: 1,
            value: 1.0,
            category: ExpenseCategoryEnum.FOOD),
        newExpense: Expense(
            date: date,
            description: "Test",
            id: 1,
            value: 1.5,
            category: ExpenseCategoryEnum.FOOD));
    when(() => repository.updateExpense(params: params))
        .thenAnswer((_) async => const Right(true));

    // Act
    final result = await usecase(params: params);
    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), true);
  });

  test("Should return Left and NotChangedExpenseDomainException when updating with same values", () async {
    // Arrange
    final date = DateTime.now();
    final params = UpdateExpenseParams(
        oldExpense: Expense(
            date: date,
            description: "Test",
            id: 1,
            value: 1.0,
            category: ExpenseCategoryEnum.FOOD),
        newExpense: Expense(
            date: date,
            description: "Test",
            id: 1,
            value: 1.0,
            category: ExpenseCategoryEnum.FOOD));

    // Act
    final result = await usecase(params: params);
    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<NotChangedExpenseDomainException>());
  });

  test("Should return Left and NotSameExpenseDomainException when not same id", () async {
    // Arrange
    final date = DateTime.now();
    final params = UpdateExpenseParams(
        oldExpense: Expense(
            date: date,
            description: "Test",
            id: 1,
            value: 1.0,
            category: ExpenseCategoryEnum.FOOD),
        newExpense: Expense(
            date: date,
            description: "Test",
            id: 2,
            value: 1.5,
            category: ExpenseCategoryEnum.FOOD));

    // Act
    final result = await usecase(params: params);
    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<NotSameExpenseDomainException>());
  });
}
