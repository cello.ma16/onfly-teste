import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/domain/repositories/i_expenses_repository.dart';
import 'package:onfly_teste/app/modules/expenses/domain/usecases/get_expenses_usecase.dart';

class MockExpenseRepository extends Mock implements IExpensesRepository {}

void main(){
  final IExpensesRepository repository = MockExpenseRepository();
  final IGetExpensesUsecase usecase = GetExpensesUsecase(expensesRepository: repository);

  test("Should return a list of Expenses", () async {
    // Arrange
    when(() => repository.getExpenses()).thenAnswer((_) async => const Right(<Expense>[]));
    // Act
    final result = await usecase();
    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<Expense>>());
  });
}