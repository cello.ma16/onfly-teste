import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/expenses/external/datasources/expense_datasource.dart';
import 'package:onfly_teste/app/modules/expenses/infra/datasources/i_expense_datasource.dart';
import 'package:onfly_teste/app/modules/expenses/params/save_expense_params.dart';
import 'package:onfly_teste/app/modules/expenses/params/update_expense_params.dart';

class MockExpenseDao extends Mock implements IExpenseDao {}

void main() {
  final IExpenseDao dao = MockExpenseDao();
  final IExpenseDatasource datasource = ExpenseDatasource(expenseDao: dao);
  test("Should return a list of Expense", () async {
    // Arrange
    when(() => dao.getExpenses()).thenAnswer((_) async => []);
    // Act
    final result = await datasource.getExpenses();
    // Assert
    expect(result, isA<List<Expense>>());
  });

  test("Should call save from dao with right params as map", () async {
    // Arrange
    final params = SaveExpenseParams(
      date: DateTime.now(),
        description: "Test", value: 10, category: ExpenseCategoryEnum.FOOD);
    when(() => dao.saveExpense(params: params.toMap()))
        .thenAnswer((_) async {});
    // Act
    await datasource.saveExpense(params: params);
    // Assert
    verify(() => dao.saveExpense(params: params.toMap())).called(1);
  });

  test("Should call update from dao with new expense as map", () async {
    // Arrange
    final params = UpdateExpenseParams(
        oldExpense: Expense(
            id: 1,
            description: "Test",
            value: 10,
            category: ExpenseCategoryEnum.FOOD,
            date: DateTime.now()),
        newExpense: Expense(
            id: 1,
            description: "Test",
            value: 30,
            category: ExpenseCategoryEnum.FOOD,
            date: DateTime.now()));

    when(() => dao.updateExpense(params: params.newExpense.toMap()))
        .thenAnswer((_) async {});

    // Act
    await datasource.updateExpense(params: params);

    // Assert
    verify(() => dao.updateExpense(params: params.newExpense.toMap())).called(1);
  });
}
