import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/user.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/authentication/i_authentication_dao.dart';
import 'package:onfly_teste/app/modules/authentication/external/datasources/authentication_datasource.dart';
import 'package:onfly_teste/app/modules/authentication/infra/datasource/i_authentication_datasource.dart';
import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';

class MockAuthenticationDao extends Mock implements IAuthenticationDao{}

void main(){
  final IAuthenticationDao dao = MockAuthenticationDao();
  final IAuthenticationDatasource datasource = AuthenticationDatasource(authenticationDao: dao);

  test("Should return a user", () async {
    // Arrange
    final email = faker.internet.email();
    final password = faker.internet.password();
    final params = SigninParams(email: email, password: password);
    final resultMap = {
      "id": faker.guid.guid(),
      "name": faker.internet.userName(),
      "email": faker.internet.email()
    };
    when(() => dao.signin(params)).thenAnswer((_) async => resultMap);

    // Act
    final result = await datasource.signin(params);

    // Assert
    expect(result, isA<User>());
    expect(result.id, resultMap["id"]);
    expect(result.name, resultMap["name"]);
    expect(result.email, resultMap["email"]);
  });


  test("Should throw a InvalidCredentials when no user was found", () async {
    // Arrange
    final email = faker.internet.email();
    final password = faker.internet.password();
    final params = SigninParams(email: email, password: password);
    when(() => dao.signin(params)).thenAnswer((_) async => null);

    // Act
    final result = datasource.signin(params);

    // Assert
    expect(result, throwsA(isA<Exception>()));
  });
}