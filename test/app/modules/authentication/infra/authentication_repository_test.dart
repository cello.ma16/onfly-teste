import 'package:dartz/dartz.dart';
import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/user.dart';
import 'package:onfly_teste/app/modules/authentication/domain/repositories/i_authentication_repository.dart';
import 'package:onfly_teste/app/modules/authentication/exceptions/domain_exceptions.dart';
import 'package:onfly_teste/app/modules/authentication/infra/datasource/i_authentication_datasource.dart';
import 'package:onfly_teste/app/modules/authentication/infra/repositories/authentication_repository.dart';
import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';

class MockAuthenticationDatasource extends Mock
    implements IAuthenticationDatasource {}

void main() {
  final IAuthenticationDatasource datasource = MockAuthenticationDatasource();
  final IAuthenticationRepository repository =
      AuthenticationRepository(authenticationDatasource: datasource);

  test('Should return a User when email and password not empty', () async {
    // Arrange
    final email = faker.internet.email();
    final password = faker.internet.password();
    final ISigninParams params = SigninParams(email: email, password: password);

    when(() => datasource.signin(params)).thenAnswer((_) async => User(
          id: 1,
          name: faker.person.name(),
          email: faker.internet.email(),
        ));

    // Act
    final result = await repository.signin(params);

    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<User>());
  });

  test("Should return a UnexpectedException when some Exception is thrown by Datasource", () async {
    // Arrange
    final email = faker.internet.email();
    final password = faker.internet.password();
    final ISigninParams params = SigninParams(email: email, password: password);

    when(() => datasource.signin(params)).thenThrow(Exception());

    // Act
    final result = await repository.signin(params);

    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<UnexpectedException>());
  });
}
