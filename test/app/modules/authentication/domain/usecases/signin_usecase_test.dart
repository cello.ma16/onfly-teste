import 'package:dartz/dartz.dart';
import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/user.dart';
import 'package:onfly_teste/app/modules/authentication/domain/repositories/i_authentication_repository.dart';
import 'package:onfly_teste/app/modules/authentication/domain/usecases/signin_usecase.dart';
import 'package:onfly_teste/app/modules/authentication/params/signin_params.dart';

class MockAuthenticationRepository extends Mock
    implements IAuthenticationRepository {}

void main() {
  final IAuthenticationRepository repository = MockAuthenticationRepository();
  final ISigninUsecase usecase = SigninUsecase(
    authenticationRepository: repository,
  );

  test('Should return a User when email and password not empty', () async {
    // Arrange
    final email = faker.internet.email();
    final password = faker.internet.password();
    final ISigninParams params = SigninParams(email: email, password: password);
    when(() => repository.signin(params)).thenAnswer((_) async => Right(User(
      id: 1,
      name: faker.person.name(),
      email: faker.internet.email(),
    )));

    // Act
    final result = await usecase(params);

    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<User>());
  });
}
