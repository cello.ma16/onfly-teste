import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/expense/i_expense_dao.dart';
import 'package:onfly_teste/app/modules/dashboard/external/datasources/dashboard_expenses_datasource.dart';
import 'package:onfly_teste/app/modules/dashboard/infra/datasources/i_dashboard_expenses_datasource.dart';
import 'package:onfly_teste/app/modules/dashboard/params/get_expenses_between_dates_params.dart';

class MockExpenseDao extends Mock implements IExpenseDao {}

void main() {
  final IExpenseDao expenseDao = MockExpenseDao();
  final IDashboardExpensesDatasource datasource =
      DashboardExpensesDatasource(expenseDao: expenseDao);

  test("Should return a list of expenses", () async {
    // Arrange
    final params = GetExpensesBetweenDatesParams(
        startDate: DateTime(2023), endDate: DateTime(2024));
    when(() => expenseDao.getExpensesBetweenDates(
            startDate: params.startDate, endDate: params.endDate))
        .thenAnswer((_) async => [
              {
                "id": "1",
                "description": "Almoço no aeroporto",
                "value": 50.0,
                "date": "2022-12-12",
                "category": "FOOD"
              }
            ]);
    // Act
    final result = await datasource.getExpensesBetweenDates(params: params);
    // Assert
    expect(result, isA<List<Expense>>());
    verify(() => expenseDao.getExpensesBetweenDates(
        startDate: params.startDate, endDate: params.endDate));
  });
}
