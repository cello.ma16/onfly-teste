import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/dashboard/domain/repositories/dashboard_expenses_repository.dart';
import 'package:onfly_teste/app/modules/dashboard/exceptions/dashboard_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/dashboard/infra/datasources/i_dashboard_expenses_datasource.dart';
import 'package:onfly_teste/app/modules/dashboard/infra/repositories/dashboard_expenses_repository.dart';
import 'package:onfly_teste/app/modules/dashboard/params/get_expenses_between_dates_params.dart';

class MockDashboardExpensesDatasource extends Mock implements IDashboardExpensesDatasource {}

void main(){

  final IDashboardExpensesDatasource datasource = MockDashboardExpensesDatasource();
  final IDashboardExpensesRepository repository = DashboardExpensesRepository(dashboardExpensesDatasource: datasource);

  test("Should return Right and List of Expenses", () async {
    // Arrange
    final params = GetExpensesBetweenDatesParams(startDate: DateTime(2023), endDate: DateTime(2024));
    when(() => datasource.getExpensesBetweenDates(params: params)).thenAnswer((_) async => []);
    // Act
    final result = await repository.getExpensesBetweenDates(params: params);
    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<Expense>>());
  });

  test("Should return Left and DashboardUnexpectedDomainException", () async {
    // Arrange
    final params = GetExpensesBetweenDatesParams(startDate: DateTime(2023), endDate: DateTime(2024));
    when(() => datasource.getExpensesBetweenDates(params: params)).thenThrow(Exception());
    // Act
    final result = await repository.getExpensesBetweenDates(params: params);
    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<DashboardUnexpectedDomainException>());
  });
}