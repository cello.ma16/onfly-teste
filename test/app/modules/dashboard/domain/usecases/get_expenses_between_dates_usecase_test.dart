import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/expense.dart';
import 'package:onfly_teste/app/modules/dashboard/domain/repositories/dashboard_expenses_repository.dart';
import 'package:onfly_teste/app/modules/dashboard/domain/usecases/get_expenses_between_dates_usecase.dart';
import 'package:onfly_teste/app/modules/dashboard/exceptions/dashboard_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/dashboard/params/get_expenses_between_dates_params.dart';

class MockDashboardExpensesRepository extends Mock
    implements IDashboardExpensesRepository {}

void main() {
  final IDashboardExpensesRepository repository =
      MockDashboardExpensesRepository();
  final IGetExpensesBetweenDatesUsecase usecase =
      GetExpensesBetweenDatesUsecase(dashboardExpensesRepository: repository);

  test("Should return a list of expenses", () async {
    // Arrange
    final params = GetExpensesBetweenDatesParams(
        startDate: DateTime(2023), endDate: DateTime(2024));
    when(() => repository.getExpensesBetweenDates(params: params))
        .thenAnswer((_) async => const Right(<Expense>[]));
    // Act
    final result = await usecase(params: params);
    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<Expense>>());
  });


  test("Should return a DashboardWrongDatesDomainException", () async {
    // Arrange
    final dateTime = DateTime.now();

    final params = GetExpensesBetweenDatesParams(
        startDate: dateTime, endDate: dateTime);

    when(() => repository.getExpensesBetweenDates(params: params))
        .thenAnswer((_) async => const Right(<Expense>[]));

    // Act
    final result = await usecase(params: params);

    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<DashboardWrongDatesDomainException>());
  });
}
