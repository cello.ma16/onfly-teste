import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/travel.dart';
import 'package:onfly_teste/app/modules/travels/domain/repositories/i_travels_repository.dart';
import 'package:onfly_teste/app/modules/travels/exceptions/travels_domain_exceptions.dart';
import 'package:onfly_teste/app/modules/travels/infra/datasources/i_travels_datasource.dart';
import 'package:onfly_teste/app/modules/travels/infra/repositories/travels_repository.dart';

class MockTravelsDatasource extends Mock implements ITravelsDatasource {}

void main(){
  final ITravelsDatasource datasource = MockTravelsDatasource();
  final ITravelsRepository repository = TravelsRepository(travelsDatasource: datasource);

  test("Should return a List with Travels", () async {
    // Arrange
    when(() => datasource.getTravels()).thenAnswer((_) async => <Travel>[]);

    // Act
    final result = await repository.getTravels();

    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<Travel>>());
  });

  test("Should return a TravelsUnexpectedException when some Exception is thrown", () async {
    // Arrange
    when(() => datasource.getTravels()).thenThrow(Exception());

    // Act
    final result = await repository.getTravels();

    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<TravelsUnexpectedException>());
  });
}