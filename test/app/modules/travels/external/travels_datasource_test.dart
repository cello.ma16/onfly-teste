import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/services/database/sqlite/daos/travels/i_travels_dao.dart';
import 'package:onfly_teste/app/core/domain/entities/travel.dart';
import 'package:onfly_teste/app/modules/travels/external/datasources/travels_datasource.dart';
import 'package:onfly_teste/app/modules/travels/infra/datasources/i_travels_datasource.dart';

class MockTravelsDao extends Mock implements ITravelsDao {}

void main() {
  final ITravelsDao dao = MockTravelsDao();
  final ITravelsDatasource datasource = TravelsDatasource(travelsDao: dao);
  final travelMock = {
    'id': '1',
    'cityFrom': 'Belo Horizonte',
    'cityTo': 'São Paulo',
    'date': "2024-05-17T00:00:00.000Z",
    'boardingPass': {
      'idTravel': '1',
      'passengerName': 'Admin Onfly',
      'flightNumber': 'FL123',
      'seat': '12A',
      'gate': 'G5',
      'session': '123',
      'boardingTime': "2024-05-17T07:45:00.000Z",
      'departureTime': "2024-05-17T08:45:00.000Z",
      'arrivalTime': "2024-05-17T10:00:00.000Z",
      'flightCompany': 'GOL',
      'airportFrom': 'Confins',
      'airportTo': 'Congonhas',
      'airportFromCode': 'CNF',
      'airportToCode': 'CGH',
    },
  };

  test('should call the datasource with the correct params', () async {
    // arrange
    when(() => dao.getTravels()).thenAnswer((_) async => [travelMock]);

    // act
    final result = await datasource.getTravels();

    // assert
    expect(result, isA<List<Travel>>());
  });
}
