import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:onfly_teste/app/core/domain/entities/travel.dart';
import 'package:onfly_teste/app/modules/travels/domain/repositories/i_travels_repository.dart';
import 'package:onfly_teste/app/modules/travels/domain/usecases/get_travels_usecase.dart';
import 'package:onfly_teste/app/modules/travels/exceptions/travels_domain_exceptions.dart';

class MockTravelsRepository extends Mock implements ITravelsRepository{}

void main(){

  final ITravelsRepository repository = MockTravelsRepository();
  final IGetTravelsUsecase usecase = GetTravelsUsecase(
    travelsRepository: repository
  );

  test("Should return a list of TRAVELS", () async {
    // Arrange
    when(() => repository.getTravels()).thenAnswer((_) async => const Right(<Travel>[]));

    // Act
    final result = await usecase();

    // Assert
    expect(result.isRight(), true);
    expect(result.fold(id, id), isA<List<Travel>>());
  });

  test("Should return a TravelsDomainException", () async {
    // Arrange
    when(() => repository.getTravels()).thenAnswer((_) async => Left(TravelsUnexpectedException()));

    // Act
    final result = await usecase();

    // Assert
    expect(result.isLeft(), true);
    expect(result.fold(id, id), isA<TravelsDomainException>());
  });
}