# Instalação e Execução do Projeto Flutter

Este guia irá ajudá-lo a instalar o Flutter na versão 3.19.5, executar o projeto e rodar os testes.

## Pré-requisitos

Antes de começar, certifique-se de que você tem o Android Studio e um emulador Android instalados. Se você ainda não os tem, você pode baixar o Android Studio [aqui](https://developer.android.com/studio) e seguir as instruções para instalar um emulador Android [aqui](https://developer.android.com/studio/run/managing-avds).

## Instalação do Flutter

1. Primeiro, baixe o Flutter SDK da versão 3.19.5 a partir do seguinte link: [Flutter SDK Archive](https://flutter.dev/docs/development/tools/sdk/archive).

2. Extraia o arquivo zip em um diretório apropriado no seu computador. Por exemplo, você pode criar um diretório chamado `flutter` no seu diretório home.

3. Adicione o flutter ao seu PATH:

   - No Windows, atualize a variável de ambiente `Path` para incluir o caminho completo para o diretório `flutter\bin`.

   - No macOS e no Linux, atualize o arquivo `~/.bashrc` ou `~/.zshrc` para incluir a linha: `export PATH="$PATH:`<caminho_para_o_diretório_flutter>`/flutter/bin"`.

4. Verifique se a instalação foi bem-sucedida executando o seguinte comando no terminal:

    ```bash
    flutter --version
    ```

   Você deve ver a versão 3.19.5 do Flutter.

## Executando o Projeto

1. Navegue até o diretório do projeto:

    ```bash
    cd <caminho_para_o_diretório_do_projeto>
    ```

2. Execute o projeto:

    ```bash
    flutter run
    ```

## Executando os Testes

Para executar os testes no Flutter, você pode usar o seguinte comando:

```bash
flutter test